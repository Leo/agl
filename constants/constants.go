package constants

import (
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/gookit/color"
	"github.com/spf13/viper"
)

var (
	// ColorBold is used to emphasize important elements, also helps us avoid usage of color. in the codebase
	ColorBold = color.Bold
	// ColorPositive is Forest Green, emphasize the message is positive, like being subscribed to an issue
	ColorPositive = color.RGB(34, 139, 34, false)
	// ColorWarn is Dark Orange, emphasize something possibly gone wrong
	ColorWarn = color.RGB(255, 140, 0, false)
	// ColorNegative is Red, emphasize negative (closed) or serious (confidential) aspects
	ColorNegative = color.RGB(220, 20, 20, false)
	// ColorUser is Chocolate, refering to someone's UserName or Email (actual Name is not used)
	ColorUser = color.RGB(210, 105, 30, false)
	// ColorObject is Steel Blue, referring to objects like commits or URLs for sources and assets
	ColorObject = color.RGB(70, 130, 180, false)
	// ColorTime is Goldenrod, referring to time
	ColorTime = color.RGB(218, 165, 32, false)

	// Hosts is a list of supported hosts, they are showed to the user
	Hosts = []string{
		"gitlab.alpinelinux.org",
		"gitlab.com",
		"gitlab.freedesktop.org",
		"gitlab.gnome.org",
	}
)

func getEditor() (string, error) {
	editor := viper.GetString("editor")
	if editor == "" {
		editor = os.Getenv("EDITOR")
		if editor == "" {
			editor = "vim"
		}
	}
	editorCmd, err := exec.LookPath(editor)
	if err != nil {
		return "", err
	}
	return editorCmd, nil
}

// EditWithEditor opens an empty file with the editor and captures what is written to it, returning a string
func EditWithEditor(def *string) (*string, error) {
	editor, err := getEditor()
	if err != nil {
		return nil, err
	}

	// Create temporary file, use XDG_CACHE_HOME/agl or $HOME/.cache/agl
	// this works around the fact some people use flatpaks which means
	// we can't use TMPDIR which is either /tmp or /run
	tempPath := os.Getenv("XDG_CACHE_HOME")
	if tempPath == "" {
		tempPath, _ = os.UserHomeDir()
		tempPath = filepath.Join(tempPath, ".cache")
	}

	// Create the directory for the cache and all leading ones
	err = os.MkdirAll(filepath.Join(tempPath, "agl"), 0771)
	if err != nil {
		return nil, err
	}

	// Create the temporary file inside ${XDG_CACHE_HOME:-$HOME/.cache}/agl
	tempFile, err := ioutil.TempFile(filepath.Join(tempPath, "agl"), "agl-*")
	if err != nil {
		return nil, err
	}

	// We were given a non-null string, write it then open the file
	if def != nil {
		tempFile.Write([]byte(*def))
	}

	filename := tempFile.Name()

	// Defer removal in case something goes wrong
	defer os.Remove(filename)

	// TODO: fix this with vscode which needs --wait flag otherwise it will return early
	cmd := exec.Command(editor, filename)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	// Close the file, we don't to read it and ioutil.ReadFile will open it again
	if err = tempFile.Close(); err != nil {
		return nil, err
	}

	// Run the editor
	err = cmd.Run()
	if err != nil {
		return nil, err
	}

	// Read the contents of the file
	resultBytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	// Convert Bytes into string
	resultDescription := string(resultBytes)
	return &resultDescription, nil
}
