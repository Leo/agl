package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.alpinelinux.org/Leo/agl/cmd/repo"
)

var repoCmd = &cobra.Command{
	Use:   "repo",
	Short: "Manage repositories",
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

func init() {
	rootCmd.AddCommand(repoCmd)

	repoCmd.AddCommand(repo.Create)
	repoCmd.AddCommand(repo.Delete)
	repoCmd.AddCommand(repo.Clone)
	repoCmd.AddCommand(repo.Fork)
}
