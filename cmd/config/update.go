package config

import (
	"fmt"
	"os"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

var (
	// Update updates a profile username and user_id by calling the GitLab API
	Update = &cobra.Command{
		Use:   "update",
		Short: "update username and user_id of a profile",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			if err := updateProfile(); err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
		},
	}
)

func init() {
	Update.Flags().StringVarP(
		&profile,
		"profile",
		"p",
		"",
		"profile to update",
	)
	Update.MarkFlagRequired("profile")
}

func updateProfile() error {
	viper.Set("profile", profile)
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}
	fmt.Printf("%s fetching current user of profile %s\n", promptui.IconInitial, profile)
	res, _, err := GitlabClient.Users.CurrentUser()
	if err != nil {
		return err
	}
	fmt.Printf("%s fetched current user of profile %s\n", promptui.IconGood, profile)

	viper.Set("profiles."+profile+".user_id", res.ID)
	fmt.Printf("%s applied %d as user_id of profile %s\n", promptui.IconGood, res.ID, profile)

	viper.Set("profiles."+profile+".username", res.Username)
	fmt.Printf("%s applied %s as username of profile %s\n", promptui.IconGood, res.Username, profile)

	cleanTemporaryViperVars()
	fmt.Printf("%s writing to configuration in %s\n", promptui.IconInitial, viper.ConfigFileUsed())
	viper.WriteConfig()
	if err != nil {
		return err
	}
	fmt.Printf("%s written to configuration in %s\n", promptui.IconGood, viper.ConfigFileUsed())

	return nil
}
