package config

import (
	"fmt"
	"os"
	"strings"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.alpinelinux.org/Leo/agl/constants"
)

type vBinds map[string]string   // equivalent to Binds map in package cmd
type vProfile map[string]string // equivalent to Profile struct in package cmd
type vConfig map[string]vProfile

func (m vProfile) checkToken(profileName string) {
	v, ok := m["token"]
	if !ok {
		warn(fmt.Sprintf("profile %s: token is not set\n", profileName))
	}
	if v == "" {
		warn(fmt.Sprintf("profile %s: token is empty\n", profileName))
	}
}

func (m vProfile) checkHost(profileName string) {
	v, ok := m["host"]
	if !ok {
		warn(fmt.Sprintf("profile %s: host is not set\n", profileName))
	}
	if strings.Contains(v, "https://") {
		warn(fmt.Sprintf("profile %s: host has schema https:// which was removed in v15", profileName))
	}
	if v == "" {
		warn(fmt.Sprintf("profile %s: host is empty\n", profileName))
		return
	}
	for _, host := range constants.Hosts {
		if host == v {
			return
		}
	}
	warn(fmt.Sprintf("profile %s: host %s is not recognized\n", profileName, v))
}

func (m vProfile) checkUserID(profileName string) {
	v, ok := m["user_id"]
	if !ok {
		warn(fmt.Sprintf("profile %s: user_id is not set\n", profileName))
	}
	if v == "" {
		warn(fmt.Sprintf("profile %s: user_id is empty\n", profileName))
	}
}

func (m vProfile) checkUsername(profileName string) {
	v, ok := m["username"]
	if !ok {
		warn(fmt.Sprintf("profile %s: username is not set\n", profileName))
	}
	if v == "" {
		warn(fmt.Sprintf("profile %s: username is empty\n", profileName))
	}
}

var (
	// Lint configuration file
	Lint = &cobra.Command{
		Use:   "lint",
		Short: "lint current configuration for mistakes and improvements",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			if err := lint(); err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
		},
	}
)

func lint() error {
	// Unmarshal the profiles struct into a map[string]map[string]string
	// The first map holds the name and refers to a map that holds the
	// keys and values, while the second one only has string and int
	// which viper converts to strings automatically
	var profiles vConfig
	err := viper.UnmarshalKey("profiles", &profiles)
	if err != nil {
		return err
	}

	var binds vBinds
	err = viper.UnmarshalKey("binds", &binds)
	if err != nil {
		return err
	}

	// Check that default_profile is set
	if viper.GetString("default_profile") == "" {
		warn("default_profile is not set")
	}

	// Loop over each profile
	for name, profile := range profiles {
		profile.checkToken(name)
		profile.checkHost(name)
		profile.checkUserID(name)
		profile.checkUsername(name)
	}

	// Loop over each bind
	for path, profileName := range binds {
		var ok bool
		if _, ok = profiles[profileName]; !ok {
			warn(fmt.Sprintf("path %s binds to non-existing profile %s", path, profileName))
		}
	}

	return nil
}

func warn(i string) {
	fmt.Printf("%s  %s\n", promptui.IconWarn, i)
}
