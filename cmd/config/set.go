package config

import (
	"fmt"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// Set a get value from a repository
var (
	Set = &cobra.Command{
		Use:   "set <KEY> <VALUE>",
		Short: "set key in configuration to value and save it",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			setKey(args[0], args[1])
		},
	}
)

func init() {
	Set.Flags().StringVarP(
		&profile,
		"profile",
		"p",
		"",
		"profile to get key in",
	)
}

func setKey(key string, value string) {
	if profile == "" {
		viper.Set(key, value)
	} else {
		if key == "user_id" || key == "username" {
			fmt.Printf("%s key %s is automatically updated via calls to the GitLab API\n",
				promptui.IconBad,
				key,
			)
			return
		}
		viper.Set("profiles."+profile+"."+key, value)
	}
	// Unset the temporary variables one can pass otherwise we will write it to the configuration
	// file, in the future we might want a helpers.WriteConfig
	cleanTemporaryViperVars()
	viper.WriteConfig()
}
