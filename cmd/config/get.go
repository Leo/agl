package config

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// Get a key value from a repository
var (
	path bool

	Get = &cobra.Command{
		Use:   "get [KEY]",
		Short: "get the value of a key or all keys if none is given",
		Long: `get the value of a key in the root of the document or all keys if no key name is given

At the moment it won't work with keys inside structures like "foo.bar", but "foo" will print all values
as a map`,
		Args: cobra.MaximumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			if path {
				fmt.Printf("%s\n", viper.ConfigFileUsed())
			} else if len(args) == 0 {
				configGetAllKeys()
			} else {
				configGetKey(args[0])
			}
		},
	}
)

func init() {
	Get.Flags().BoolVar(
		&path,
		"path",
		false,
		"Show path to the configuration file being used",
	)
	Get.Flags().StringVarP(
		&profile,
		"profile",
		"p",
		"",
		"profile to get key in",
	)
}

func configGetAllKeys() {
	if profile != "" {
		subViper := viper.Sub("profiles." + profile)
		for _, key := range subViper.AllKeys() {
			fmt.Printf("%s -> %v\n", key, subViper.Get(key))
		}
	} else {
		for _, key := range viper.AllKeys() {
			fmt.Printf("%s -> %v\n", key, viper.Get(key))
		}
	}
}

func configGetKey(key string) {
	if profile != "" {
		subViper := viper.Sub("profiles." + profile)
		fmt.Printf("%s -> %v\n", key, subViper.Get(key))
	} else {
		fmt.Printf("%s -> %v\n", key, viper.Get(key))
	}
}
