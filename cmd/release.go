package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.alpinelinux.org/Leo/agl/cmd/release"
)

var releaseCmd = &cobra.Command{
	Use:   "release",
	Short: "manage releases",
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

func init() {
	rootCmd.AddCommand(releaseCmd)

	releaseCmd.AddCommand(release.Create)
	releaseCmd.AddCommand(release.Delete)
	releaseCmd.AddCommand(release.List)
	releaseCmd.AddCommand(release.View)
	releaseCmd.AddCommand(release.Edit)
}
