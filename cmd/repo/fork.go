package repo

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

// Fork a repository (or project in GitLab terms)
var Fork = &cobra.Command{
	Use:   "fork",
	Short: "Create fork of a repository",
	Long: `Create fork of a repository:
	
The repository to be forked can be given in any of the 3 ways:
- Full URL via --repo: e.g. https://gitlab.alpinelinux.org/Leo/agl
- The [NAMESPACE/]PATH specification via --repo: e.g. --repo Leo/agl
- The URL as acquired from the remote of the repo we currently reside, can be influenced with --remote
`,
	Args: cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		if err := forkRepo(); err != nil {
			fmt.Printf("%s %s\n", promptui.IconBad, err)
			os.Exit(1)
		}
	},
}

func init() {
	Fork.Flags().BoolVarP(&dryRun, "dry-run", "n", false, "don't fork, show which project it would fork")
}

func forkRepo() error {
	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}

	// Ask user for Path
	askPath := promptui.Prompt{
		Label:   "Path of the fork (used in URL)",
		Default: strings.Split(repoSpec, "/")[1],
	}
	path, err := askPath.Run()
	if err != nil {
		return err
	}

	// Ask user for the Name
	askName := promptui.Prompt{
		Label: "Name of the fork (shown in Web Interface)",
		// This is OK because path overrides the default when making the API call
		Default: path,
	}
	name, err := askName.Run()
	if err != nil {
		return err
	}

	if repoSpec == fmt.Sprintf("%s/%s", helpers.ProfileGet("username"), path) {
		return errors.New("can't fork a repo into itself")
	}

	if dryRun {
		fmt.Printf("Forking %s into %s/%s\n",
			repoSpec,
			helpers.ProfileGet("username"),
			path,
		)
		return nil
	}
	fmt.Printf("%s Forking %s into %s/%s\n",
		promptui.IconInitial,
		repoSpec,
		helpers.ProfileGet("username"),
		path,
	)
	res, _, err := GitlabClient.Projects.ForkProject(
		repoSpec,
		&gitlab.ForkProjectOptions{
			Name: &name,
			Path: &path,
		},
		nil)
	if err != nil {
		return err
	}
	fmt.Printf("%s forked %s into %s, with Display Name %s\n",
		promptui.IconGood,
		repoSpec,
		res.PathWithNamespace,
		res.Name,
	)
	return nil
}
