package repo

import (
	"fmt"
	"os"
	"strings"
	"sync"

	git "github.com/libgit2/git2go/v31"
	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

var (
	waitGroup sync.WaitGroup

	// Clone a repository (or Project in GitLab terms)
	Clone = &cobra.Command{
		Use:   "clone [[NAMESPACE/]PATH || URL]",
		Short: "clone a repository via path or URL",
		Long: `clones a repository to a directory named after the path, setting up origin and upstream
remotes.
If NAMESPACE is omitted then the current authenticated user is used.`,
		Args: cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			// It is ok to use args[0] because we have only 1 positional arg that is valid
			if err := cloneRepo(strings.Split(args[0], "/")); err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
		},
	}
)

func init() {
	Clone.Flags().BoolVarP(&dryRun, "dry-run", "n", false, "don't create the issue, show what it would look like")
}

func requestRepoURL(GitlabClient *gitlab.Client, pid int, channel chan *string) {
	defer waitGroup.Done() // Schedule call to the end of this function or whenever we return

	res, _, err := GitlabClient.Projects.GetProject(
		pid,
		&gitlab.GetProjectOptions{},
		nil,
	)
	if err != nil {
		fmt.Printf("%s couldn't get URL of upstream repo, won't be able to set up upstream remote\n", promptui.IconWarn)
		channel <- nil
		return
	}
	channel <- &res.HTTPURLToRepo
}

func cloneRepo(input []string) error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}
	var repoSpec []string

	if len(input) == 1 {
		repoSpec = []string{helpers.ProfileGet("username"), input[0]}
	} else if len(input) == 2 {
		repoSpec = input
	} else {
		// Get the last 2 elements of the path component of the URL
		repoSpec = []string{input[len(input)-2], input[len(input)-1]}
	}

	// Ask the GitLab API for repo information
	res, _, err := GitlabClient.Projects.GetProject(
		strings.Join(repoSpec, "/"),
		&gitlab.GetProjectOptions{},
		nil,
		nil,
	)
	if err != nil {
		return err
	}

	// Set cloneURL to https value and also callback to use the HTTPS user-pass authentication
	cloneURL := res.HTTPURLToRepo
	credCall := git.RemoteCallbacks{
		CredentialsCallback: helpers.GitCredentials,
	}

	// Channel for our goroutine to send data over to us
	upstreamRemoteURL := make(chan *string)

	// Project is Forked, start goroutine to request information from upstream
	if res.ForkedFromProject != nil {
		waitGroup.Add(1)
		go requestRepoURL(
			GitlabClient,
			res.ForkedFromProject.ID,
			upstreamRemoteURL)
	}

	if err != nil {
		return err
	}

	if dryRun {
		fmt.Printf("Would clone %s into %s\n",
			cBold.Sprint(cloneURL),
			cBold.Sprint(repoSpec[1]),
		)
		return nil
	}
	fmt.Printf("%s cloning %s (%s) into %s\n",
		promptui.IconInitial,
		cBold.Sprint(strings.Join(repoSpec, "/")),
		cBold.Sprint(cloneURL),
		cBold.Sprint(repoSpec[1]),
	)
	repo, err := git.Clone(
		cloneURL,
		repoSpec[1],
		&git.CloneOptions{
			FetchOptions: &git.FetchOptions{
				RemoteCallbacks: credCall,
			},
		},
	)
	if err != nil {
		return err
	}
	fmt.Printf("%s cloned %s (%s) into %s\n",
		promptui.IconGood,
		cBold.Sprint(strings.Join(repoSpec, "/")),
		cBold.Sprint(cloneURL),
		cBold.Sprint(repoSpec[1]),
	)

	// No point in going forward if the fork failed with no data
	if res.ForkedFromProject != nil {

		// Block until we get the upstreamRemoteURL
		upstreamURL := <-upstreamRemoteURL

		if upstreamURL == nil {
			fmt.Printf("%s couldn't get upstream remote URL, can't create upstream remote\n", promptui.IconWarn)
			return nil
		}

		// Switch to the directory we cloned
		err = os.Chdir("./" + repoSpec[1])
		if err != nil {
			return err
		}
		// Set the repo called upstream with the URL we received
		_, err := repo.Remotes.Create("upstream", *upstreamURL)
		if err != nil {
			return err
		}
		fmt.Printf("%s created remote %s pointing to %s\n",
			promptui.IconGood,
			cBold.Sprint("upstream"),
			cBold.Sprint(*upstreamURL),
		)
	}
	return nil
}
