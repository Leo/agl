package repo

import (
	"fmt"
	"os"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

var (
	// Delete a repository (or Project in GitLab terms)
	Delete = &cobra.Command{
		Use:     "delete",
		Aliases: []string{"del", "rm"},
		Short:   "Delete a repository",
		Args:    cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			if err := deleteRepo(); err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
		},
	}
)

func init() {
	Delete.Flags().BoolVarP(&dryRun, "dry-run", "n", false, "don't delete repo, show which one it would delete")
}

func deleteRepo() error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}
	repoToDelete, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}
	if dryRun {
		fmt.Printf("Would delete repository %s\n", cBold.Sprint(repoToDelete))
		return nil
	}
	fmt.Printf("%s Deleting repository %s\n", promptui.IconInitial, cBold.Sprint(repoToDelete))
	_, err = GitlabClient.Projects.DeleteProject(repoToDelete, nil)
	if err != nil {
		return err
	}
	fmt.Printf("%s Deleted repo %s\n", promptui.IconGood, cBold.Sprint(repoToDelete))
	return nil
}
