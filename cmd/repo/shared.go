package repo

import "gitlab.alpinelinux.org/Leo/agl/constants"

var (
	// Shared variables used by more than 1 command in the package
	dryRun bool

	cBold = constants.ColorBold
)
