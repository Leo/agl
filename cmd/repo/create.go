package repo

import (
	"errors"
	"fmt"
	"os"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

var (
	// Create a repository (or Project in GitLab terms)
	Create = &cobra.Command{
		Use:   "create",
		Short: "Create a repository",
		Long: `Prompt the user to answer various questions like path and display name of the repository and then
creates a repository for the user`,
		Args: cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			if err := createRepo(); err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
		},
	}
)

func init() {
	Create.Flags().BoolVarP(&dryRun, "dry-run", "n", false, "don't create repo, show attributes used")
}

func askVisibility(o *gitlab.CreateProjectOptions) {
	promptVisibility := promptui.Select{
		Label: "Project Visibility",
		Items: []string{"private", "internal", "public"},
	}
	_, resultVisibility, err := promptVisibility.Run()
	if err != nil {
		fmt.Printf("%s couldn't get value, didn't change default visibility\n", promptui.IconBad)
		return
	}
	switch resultVisibility {
	case "private":
		o.Visibility = gitlab.Visibility("private")
	case "internal":
		o.Visibility = gitlab.Visibility("internal")
	case "public":
		o.Visibility = gitlab.Visibility("public")
	}
}

func askDescription(o *gitlab.CreateProjectOptions) {
	prompt := promptui.Prompt{
		Label: "Description",
	}
	result, err := prompt.Run()
	if err != nil {
		fmt.Printf("%s couldn't get value\n", promptui.IconBad)
	}
	if result != "" {
		o.Description = &result
	}
}

func askName(o *gitlab.CreateProjectOptions) {
	// Ask for display name
	askName := promptui.Prompt{
		Label: "Pick a Display Name for the repo (blank to use path)",
	}
	result, err := askName.Run()
	if err != nil {
		fmt.Printf("%s couldn't get name because of %s\n", promptui.IconBad, err)
	}
	o.Name = &result
}

// TODO: Make Validate restrict it to only stuff GitLab accepts as Path
// TODO: Make putting a blank Path make it use the name as dashed-lowercase
func askPath(o *gitlab.CreateProjectOptions) {
	// Ask for display name
	askPath := promptui.Prompt{
		Label: "Pick the name of the repo as used in the URL",
		Validate: func(s string) error {
			if s == "" {
				return errors.New("Path must not be empty")
			}
			return nil
		},
	}
	result, err := askPath.Run()
	if err != nil {
		fmt.Printf("%s couldn't get name because of %s\n", promptui.IconBad, err)
	}
	o.Path = &result
}

func createRepo() error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}

	opts := &gitlab.CreateProjectOptions{}

	/*
	 * TODO: Handle every single option through the usage of a select prompt that
	 * allows users to pick an attribute and an specific prompt for each type of
	 * attribute (example selection for a boolean or multiple-choice, and a prompt
	 * for ones that require an attribute)
	 *
	 * Implemented (Either here or elsewhere in Code due to be obligatory):
	 *  - Path
	 *  - Name
	 *  - Visibility
	 *  - Description
	 * Rejected Implementations:
	 *  - DefaultBranch (Doesn't change anything for new repos, might be approved for imports)
	 *
	 * Reference: https://godoc.org/github.com/xanzy/go-gitlab#CreateProjectOptions
	 * Reference: https://docs.gitlab.com/ee/api/projects.html#create-project
	 */
	validAttributes := []string{
		"continue",
		"path",
		"name",
		"visibility",
		"description"}
	// Handle here
Continue:
	for {
		askAttribute := promptui.Select{
			Label: "Pick an attribute of the Project to edit or continue",
			Items: validAttributes,
		}
		_, askResult, err := askAttribute.Run()
		if err != nil {
			return err
		}
		switch askResult {
		case "name":
			askName(opts)
		case "path":
			askPath(opts)
		case "visibility":
			askVisibility(opts)
		case "description":
			askDescription(opts)
		case "continue":
			break Continue
		}

		// Check if Name is empty and use Path since every Path is valid for any name
		if (opts.Path != nil && *opts.Path != "") && (opts.Name == nil || *opts.Name == "") {
			opts.Name = opts.Path
		}
	}

	// There is no chance that opts.Name will be nil while Opts.Path is not because
	// of the check in the loop above
	if (opts.Path == nil || *opts.Path == "") || (opts.Name == nil || *opts.Name == "") {
		return errors.New("Name and Path must not be blank")
	}

	if dryRun {
		fmt.Printf("Name: %s\nPath: %s\n",
			*opts.Name,
			*opts.Path,
		)
		if opts.Description != nil {
			fmt.Printf("Description %s\n", *opts.Description)
		}
		if opts.Visibility != nil {
			fmt.Printf("Visibility: %s\n", *opts.Visibility)
		}
		return nil
	}
	fmt.Printf("%s creating project %s/%s, with Display Name %s\n",
		promptui.IconInitial,
		helpers.ProfileGet("username"),
		*opts.Path,
		*opts.Name,
	)
	res, _, err := GitlabClient.Projects.CreateProject(opts, nil)
	if err != nil {
		return err
	}
	fmt.Printf("%s created project %s, with Display Name %s\n", promptui.IconGood, res.PathWithNamespace, res.Name)
	return nil
}
