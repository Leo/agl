package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.alpinelinux.org/Leo/agl/cmd/config"
)

var configCmd = &cobra.Command{
	Use:   "config",
	Short: "manage agl configuration",
	Long:  `view whole configuration, get an specific key or save an specific key`,
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

func init() {
	rootCmd.AddCommand(configCmd)
	configCmd.AddCommand(config.Set)
	configCmd.AddCommand(config.Get)
	configCmd.AddCommand(config.Lint)
	configCmd.AddCommand(config.Update)
}
