package release

import (
	"fmt"
	"os"
	"strings"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

var (
	latestRelease bool

	// View information about a release via its tag name
	View = &cobra.Command{
		Use:   "view [Tag Name]",
		Short: "View information about a Gitlab Release via its Tag Name",
		Long: `View information about a Gitlab release from a project:
		
If Tag Name is omitted then prompt for release`,
		Args: cobra.RangeArgs(0, 1),
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 0 {
				args = append(args, "")
			}
			if err := viewRelease(args[0]); err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
		},
	}
)

func init() {
	View.Flags().BoolVarP(&latestRelease,
		"latest",
		"l",
		false,
		"Get latest release of the project",
	)
}

func viewRelease(releaseTag string) error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}

	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	// Sent empty release, get latest release
	if latestRelease || releaseTag == "" {
		releases, _, err := GitlabClient.Releases.ListReleases(
			repoSpec,
			&gitlab.ListReleasesOptions{},
			nil,
		)
		if err != nil {
			return err
		}
		if len(releases) < 1 {
			return fmt.Errorf("Project %s has no releases", repoSpec)
		}
		if latestRelease {
			releaseTag = releases[0].TagName
		} else if releaseTag == "" {
			var validReleaseTags []string
			for _, release := range releases {
				validReleaseTags = append(validReleaseTags, fmt.Sprintf("Name: %s Tag: %s", release.Name, release.TagName))
			}

			askRelease := promptui.Select{
				Label: "Pick a Release",
				Items: validReleaseTags,
			}
			_, askReleaseTag, err := askRelease.Run()
			if err != nil {
				return err
			}
			releaseTag = strings.Split(askReleaseTag, " ")[3]
		}
	}
	release, _, err := GitlabClient.Releases.GetRelease(
		repoSpec,
		releaseTag,
		nil,
	)
	if err != nil {
		return err
	}

	fmt.Printf("* Release %s\n* uses Tag %s which references commit %s by %s\n* created on %s at %s\n",
		cBold.Sprint(release.Name),
		cBold.Sprint(release.TagName),
		cObject.Sprint(release.Commit.ShortID),
		cUser.Sprint(release.Commit.AuthorName),
		cTime.Sprint(release.CreatedAt.Format("Monday 2 January 2006")),
		cTime.Sprint(release.CreatedAt.Format("15:04:05")),
	)

	// Possible for releases to not have any owners
	if release.Author.Name != "" {
		fmt.Printf("* created by %s\n",
			cUser.Sprint(release.Author.Username),
		)
	} else {
		fmt.Printf("%s no author information, release auto created via git tag push\n", cWarn.Sprint("*"))
	}

	if release.Description != "" {
		fmt.Println("* Description:")
		for _, line := range strings.Split(release.Description, "\n") {
			fmt.Printf("  %s\n", line)
		}
	}

	fmt.Println("* Assets:\n  * Sources:")
	for _, source := range release.Assets.Sources {
		fmt.Printf("    %s (format: %s)\n",
			cObject.Sprint(source.URL),
			cBold.Sprint(source.Format),
		)
	}
	if len(release.Assets.Links) >= 1 {
		fmt.Println("  * Links:")
		for _, asset := range release.Assets.Links {
			fmt.Println("    -")
			fmt.Printf("      Name: %s\n", asset.Name)
			fmt.Printf("      URL: %s\n", asset.URL)
		}
	}
	return nil
}
