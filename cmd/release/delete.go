package release

import (
	"fmt"
	"os"
	"strings"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

// Delete a release
var Delete = &cobra.Command{
	Use:     "delete [Release Tag]",
	Aliases: []string{"del", "rm"},
	Short:   "Delete a release",
	Long:    `Delete a release, if none is given prompt the user to pick one`,
	Args:    cobra.RangeArgs(0, 1),
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			args = append(args, "")
		}
		if err := deleteRelease(args[0]); err != nil {
			fmt.Printf("%s %s\n", promptui.IconBad, err)
			os.Exit(1)
		}
	},
}

func init() {
	Delete.Flags().BoolVarP(&dryRun, "dry-run", "n", false, "don't delete release, show which one it would delete")
}

func deleteRelease(releaseTag string) error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}
	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	// Sent empty release, get latest release
	if releaseTag == "" {
		releases, _, err := GitlabClient.Releases.ListReleases(
			repoSpec,
			&gitlab.ListReleasesOptions{},
			nil,
		)
		if err != nil {
			return err
		}
		if len(releases) < 1 {
			return fmt.Errorf("Project %s has no releases", repoSpec)
		}

		var validReleaseTags []string
		for _, release := range releases {
			validReleaseTags = append(validReleaseTags, fmt.Sprintf("Name: %s Tag: %s", release.Name, release.TagName))
		}

		askRelease := promptui.Select{
			Label: "Pick a Release",
			Items: validReleaseTags,
		}
		_, askReleaseTag, err := askRelease.Run()
		if err != nil {
			return err
		}
		releaseTag = strings.Split(askReleaseTag, " ")[3]
	}

	if dryRun {
		fmt.Printf("Would delete release %s from %s\n",
			cBold.Sprint(releaseTag),
			cBold.Sprint(repoSpec),
		)
		return nil
	}
	fmt.Printf("%s deleting Release %s from %s\n",
		promptui.IconInitial,
		cBold.Sprint(releaseTag),
		cBold.Sprint(repoSpec),
	)
	_, _, err = GitlabClient.Releases.DeleteRelease(
		repoSpec,
		releaseTag,
		nil,
	)
	if err != nil {
		return err
	}
	fmt.Printf("%s deleted Release %s from %s\n",
		promptui.IconGood,
		cBold.Sprint(releaseTag),
		cBold.Sprint(repoSpec),
	)
	return nil
}
