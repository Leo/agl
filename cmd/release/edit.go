package release

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/constants"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

var (
	// Edit title, description or both attributes of a merge request
	Edit = &cobra.Command{
		Use:   "edit [Release Tag]",
		Short: "edit the title or description of a release",
		Long:  `edit the title or description of a release, if none is given prompt the user to pick one`,
		Args:  cobra.RangeArgs(0, 1),
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 0 {
				args = append(args, "")
			}
			if err := editRelease(args[0]); err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
		},
	}
)

func editRelease(releaseTag string) error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}
	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	// Sent empty release, get latest release
	if releaseTag == "" {
		releases, _, err := GitlabClient.Releases.ListReleases(
			repoSpec,
			&gitlab.ListReleasesOptions{},
			nil,
		)
		if err != nil {
			return err
		}
		if len(releases) < 1 {
			return fmt.Errorf("Project %s has no releases", repoSpec)
		}

		var validReleaseTags []string
		for _, release := range releases {
			validReleaseTags = append(validReleaseTags, fmt.Sprintf("Name: %s Tag: %s", release.Name, release.TagName))
		}

		askRelease := promptui.Select{
			Label: "Pick a Release",
			Items: validReleaseTags,
		}
		_, askReleaseTag, err := askRelease.Run()
		if err != nil {
			return err
		}
		releaseTag = strings.Split(askReleaseTag, " ")[3]
	}

	// Get the values from the release
	release, _, err := GitlabClient.Releases.GetRelease(
		repoSpec,
		releaseTag,
		nil,
	)

	validAttrs := []string{"continue", "name", "description"}
	opts := gitlab.UpdateReleaseOptions{
		Name:        &release.Name,
		Description: &release.Description,
	}

Continue:
	// Infinite loop until continue is picked
	for {
		// Select that allows users to pick any of the valid attributes to edit
		askAttributes := promptui.Select{
			Label: "Edit attributes",
			Items: validAttrs,
		}
		_, resultAttributes, err := askAttributes.Run()
		if err != nil {
			return err
		}
		// Deal with each attribute
		switch resultAttributes {
		case "continue":
			break Continue
		case "name":
			askName := promptui.Prompt{
				Label:   "name",
				Default: *opts.Name,
				Validate: func(s string) error {
					if s == "" {
						return errors.New("name must not be empty")
					}
					return nil
				},
			}
			resultName, err := askName.Run()
			if err != nil {
				fmt.Printf("%s failed to get Release name due to %s\n", promptui.IconBad, err)
				continue
			}
			opts.Name = &resultName
		case "description":
			// Use EditWithEditor to open description on one's text editor since it is multiline
			resultDescription, err := constants.EditWithEditor(opts.Description)
			if err != nil {
				fmt.Printf("%s failed to get description due to %s\n", promptui.IconBad, err)
				continue
			} else if resultDescription == nil {
				fmt.Printf("%s description is empty, not changing\n", promptui.IconWarn)
				continue
			}
			resultDesc := strings.TrimSpace(*resultDescription)
			opts.Description = &resultDesc
		}
	}
	// Don't update if both values are the same as before
	if *opts.Name == release.Name && *opts.Description == release.Description {
		return errors.New("no values were changed")
	}

	fmt.Printf("%s updating %s from %s\n", promptui.IconInitial, releaseTag, repoSpec)
	_, _, err = GitlabClient.Releases.UpdateRelease(
		repoSpec,
		releaseTag,
		&opts,
		nil,
	)
	if err != nil {
		return err
	}
	fmt.Printf("%s updated %s from %s\n", promptui.IconGood, releaseTag, repoSpec)

	return nil
}
