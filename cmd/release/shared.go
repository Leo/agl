package release

import "gitlab.alpinelinux.org/Leo/agl/constants"

var (
	dryRun bool

	cBold   = constants.ColorBold
	cUser   = constants.ColorUser
	cTime   = constants.ColorTime
	cObject = constants.ColorObject
	cWarn   = constants.ColorWarn
)
