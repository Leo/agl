package release

import (
	"fmt"
	"os"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

// List releases
var List = &cobra.Command{
	Use:   "list",
	Short: "list release(s) of a repository",
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		if err := listReleases(); err != nil {
			fmt.Printf("%s %s\n", promptui.IconBad, err)
			os.Exit(1)
		}
	},
}

func listReleases() error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}

	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	// List releases
	releases, _, err := GitlabClient.Releases.ListReleases(
		repoSpec,
		&gitlab.ListReleasesOptions{},
		nil,
	)
	if err != nil {
		return err
	}
	for _, release := range releases {
		fmt.Printf("* %s (Tag: %s)",
			cBold.Sprint(release.Name),
			release.TagName,
		)
		if release.Author.Username != "" {
			fmt.Printf(" by %s", cUser.Sprint(release.Author.Name))
		}
		fmt.Printf(" on %s at %s\n",
			cTime.Sprint(release.CreatedAt.Format("Monday 2 January 2006")),
			cTime.Sprint(release.CreatedAt.Format("15:04:05")),
		)
	}
	return nil
}
