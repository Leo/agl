package release

import (
	"errors"
	"fmt"
	"os"
	"strings"
	"sync"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/constants"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

// Create a release
var (
	waitGroup sync.WaitGroup

	Create = &cobra.Command{
		Use:   "create",
		Short: "create a release",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			if err := createRelease(); err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
		},
	}
)

func init() {
	Create.Flags().BoolVarP(&dryRun, "dry-run", "n", false, "don't create Release, show how it would look")
}

func fetchCommits(GitlabClient *gitlab.Client, repoSpec string, channel chan []gitlab.Commit) {
	defer waitGroup.Done() // Schedule call to the end of this function or whenever we return

	var t []gitlab.Commit

	res, _, err := GitlabClient.Commits.ListCommits(
		repoSpec,
		&gitlab.ListCommitsOptions{},
		nil,
	)
	if err != nil {
		channel <- t
		return
	}
	for _, commit := range res {
		if commit != nil {
			t = append(t, *commit)
		}
	}
	channel <- t
}

func fetchTags(GitlabClient *gitlab.Client, repoSpec string, channel chan []string) {
	defer waitGroup.Done() // Schedule call to the end of this function or whenever we return

	var t []string

	tags, _, err := GitlabClient.Tags.ListTags(
		repoSpec,
		&gitlab.ListTagsOptions{},
		nil,
	)
	if err != nil {
		channel <- t
		return
	}
	for _, tag := range tags {
		if tag != nil {
			t = append(t, tag.Name)
		}
	}
	channel <- t
}

func getValidTags(t []*gitlab.Tag) []string {
	var validTags []string
	for _, tag := range t {
		if tag.Release == nil {
			validTags = append(validTags, tag.Name)
		}
	}
	return validTags
}

func getValidCommits(c []gitlab.Commit) ([]string, map[string]string) {
	var validCommits []string
	validCommitsMap := make(map[string]string)
	for _, commit := range c {
		validCommits = append(validCommits, commit.Title)
		validCommitsMap[commit.Title] = commit.ID
	}
	return validCommits, validCommitsMap
}

func askCommit(validCommits []string, validCommitsMap map[string]string, opts *gitlab.CreateReleaseOptions) error {
	// Ask the user to pick a git commit initially, he can
	askCommit := promptui.Select{
		Label: "Git Commit",
		Items: validCommits,
	}
	_, resultCommit, err := askCommit.Run()
	if err != nil {
		return fmt.Errorf("failed to get Git Commit due to %s", err)
	}
	ref := validCommitsMap[resultCommit]
	opts.Ref = &ref
	return nil
}

func askTag(validTags []string, opts *gitlab.CreateReleaseOptions) error {
	askTag := promptui.Select{
		Label: "Git Tag",
		Items: validTags,
	}
	_, resultTag, err := askTag.Run()
	if err != nil {
		return fmt.Errorf("failed to get Git Tag due to %s", err)
	}
	opts.TagName = &resultTag
	return nil
}

func askTagName(opts *gitlab.CreateReleaseOptions, existingTags map[string]bool) error {
	var def string
	if opts.TagName == nil {
		def = ""
	} else {
		def = *opts.TagName
	}
	askName := promptui.Prompt{
		Label:   "Tag name (created from the chosen Git Commit)",
		Default: def,
		Validate: func(s string) error {
			if s == "" {
				return errors.New("Path must not be empty")
			}
			// Idiomatic way to check for existence of a key, ok will be true or false
			// depending on whether the key exists
			if _, ok := existingTags[s]; ok {
				return errors.New("Tag name already taken")
			}
			return nil
		},
	}
	resultName, err := askName.Run()
	if err != nil {
		return fmt.Errorf("failed to get Tag Name due to %s", err)
	}
	opts.TagName = &resultName
	return nil
}

func createRelease() error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}

	// Options to control what the release is, like Tag, Commit Reference, Assets, Name and others
	opts := gitlab.CreateReleaseOptions{}

	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	// Fetch commits on the background
	commitChannel := make(chan []gitlab.Commit)
	tagChannel := make(chan []string)
	waitGroup.Add(2)
	go fetchCommits(GitlabClient, repoSpec, commitChannel)
	go fetchTags(GitlabClient, repoSpec, tagChannel)

	askRelease := promptui.Select{
		Label: "Release Object",
		Items: []string{"Git Tag", "Git Commit (Slow on Huge repositories)"},
	}
	_, resultRelease, err := askRelease.Run()
	if err != nil {
		return err
	}

	validAttrs := []string{"continue", "Name", "Description", "Assets"}

	var validTags []string
	validTagsMap := make(map[string]bool)
	var validCommits []string
	var validCommitsMap map[string]string

	if resultRelease == "Git Tag" {

		// Pretend we started getting the tags now but just read from the channel
		// it will block until an empty slice of strings is sent or the slice of strings
		// with all valid tags is sent
		fmt.Printf("%s getting tags in repository %s\n", promptui.IconInitial, repoSpec)
		fetchedTags := <-tagChannel

		// There are no valid tags, return an error to be printed
		if len(fetchedTags) < 1 {
			return errors.New("no valid tags could be found, create a new tag with git tag and push it")
		}

		fmt.Printf("%s got tags in repository %s\n", promptui.IconGood, repoSpec)

		// There is only one tag so pick it for the user
		if len(fetchedTags) == 1 {
			opts.TagName = &fetchedTags[0]
		} else {
			// Otherwise ask the user, if the user fails return the error
			err = askTag(validTags, &opts)
			if err != nil {
				return err
			}
		}

		// The user chose to make a release based on a Git Tag so add it to the valid attributes
		// so the user can edit it again in the future
		validAttrs = append(validAttrs, "Git Tag")
	} else if resultRelease == "Git Commit (Slow on Huge repositories)" {

		// Pretend we started fetching the commits just now, while in fact we were always doing
		// it in the background, just read from the channel
		fmt.Printf("%s getting commits in repository %s\n", promptui.IconInitial, repoSpec)
		fetchedCommits := <-commitChannel
		if len(fetchedCommits) < 1 {
			// This can only reasonably be an error in fetching the commits, there is no logic
			// in making a release for an empty repository, if the user is actually trying to
			// do that then he is on his own
			return fmt.Errorf("couldn't fetch commits in repository %s", repoSpec)
		}

		fmt.Printf("%s got commits in repository %s\n", promptui.IconGood, repoSpec)

		validCommits, validCommitsMap = getValidCommits(fetchedCommits)
		validAttrs = append(validAttrs, "Git Commit")
		validAttrs = append(validAttrs, "Tag Name")

		err := askCommit(validCommits, validCommitsMap, &opts)
		if err != nil {
			return err
		}

		// Get valid tags, we need a map to fill them with
		fetchedTags := <-tagChannel
		for _, tag := range fetchedTags {
			validTagsMap[tag] = true
		}

		err = askTagName(&opts, validTagsMap)
		if err != nil {
			return err
		}
	}

	/*
	 * TODO: Handle options that GitLab API and go-gitlab implement
	 *
	 * Implemented (Either here or elsewhere in Code due to be obligatory):
	 *  - Git Commit
	 *  - Git Tag (is TagName for go-gitlab and tag_name for GitLab API)
	 *  - Name
	 *  - Description
	 *  - Assets
	 *
	 * Reference: https://godoc.org/github.com/xanzy/go-gitlab#CreateReleaseOptions
	 * Reference: https://docs.gitlab.com/ee/api/releases/#create-a-release
	 */
	// Create some empty variables for usage
	assetSlice := []string{}
	assetMap := make(map[string]gitlab.ReleaseAssetLink)

Continue:
	// Infinite loop until continue is picked
	for {
		// Select that allows users to pick any of the valid attributes to edit
		askAttributes := promptui.Select{
			Label: "Edit attributes",
			Items: validAttrs,
		}
		_, resultAttributes, err := askAttributes.Run()
		if err != nil {
			return err
		}
		// Deal with each attribute
		switch resultAttributes {
		case "continue":
			break Continue
		case "Name":
			askName := promptui.Prompt{
				Label: "Name",
				Validate: func(s string) error {
					if s == "" {
						return errors.New("Path must not be empty")
					}
					return nil
				},
			}
			resultName, err := askName.Run()
			if err != nil {
				fmt.Printf("%s failed to get Release name due to %s\n", promptui.IconBad, err)
				continue
			}
			opts.Name = &resultName
		case "Git Commit":
			err := askCommit(validCommits, validCommitsMap, &opts)
			if err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
			}
		case "Tag Name":
			err = askTagName(&opts, validTagsMap)
			if err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
			}
		case "Git Tag":
			askTag(validTags, &opts)
			if err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
			}
		case "Description":
			// Use EditWithEditor to open description on one's text editor since it is multiline
			resultDescription, err := constants.EditWithEditor(nil)
			if err != nil {
				fmt.Printf("%s failed to get description due to %s\n", promptui.IconBad, err)
				continue
			} else if resultDescription == nil {
				fmt.Printf("%s description is empty, not changing\n", promptui.IconWarn)
				continue
			}
			resultDesc := strings.TrimSpace(*resultDescription)
			opts.Description = &resultDesc
		case "Assets":
			assetSlice, assetMap, err = manageAssets(assetSlice, assetMap)
			// We have assets, create a slice of them
			if len(assetMap) > 0 {
				finalAssetSlice := []*gitlab.ReleaseAssetLink{}
				for _, asset := range assetMap {
					finalAssetSlice = append(finalAssetSlice, &asset)
				}
				opts.Assets = &gitlab.ReleaseAssets{Links: finalAssetSlice}
			}
		}
	}
	if resultRelease == "Git Tag" {
		if opts.TagName == nil || *opts.TagName == "" {
			return errors.New("A Git Tag must be set")
		}
	} else if resultRelease == "Git Commit (Slow on Huge repositories)" {
		if opts.Ref == nil || *opts.Ref == "" {
			return errors.New("A Git Commit must be set")
		}
		if opts.TagName == nil || *opts.TagName == "" {
			return errors.New("A Tag Name must be set")
		}
	}

	if dryRun {
		fmt.Printf("Tag: %s\nProject: %s\n", *opts.TagName, repoSpec)
		if opts.Name != nil {
			fmt.Printf("Name: %s\n", *opts.Name)
		}
		if opts.Description != nil {
			for _, line := range strings.Split(*opts.Description, "\n") {
				fmt.Printf("Description: %s\n", line)
			}
		}
		if opts.Ref != nil {
			fmt.Printf("Ref: %s\n", *opts.Ref)
		}
		if opts.Assets != nil {
			fmt.Println("Assets:")
			for _, asset := range opts.Assets.Links {
				fmt.Println("  -")
				fmt.Printf("    Name: %s\n", asset.Name)
				fmt.Printf("    URL: %s\n", asset.URL)
			}
		}
		return nil
	}
	if resultRelease == "Git Tag" {
		fmt.Printf("%s creating Release on %s based on Tag %s\n", promptui.IconGood, repoSpec, *opts.TagName)
	} else if resultRelease == "Git Commit (Slow on Huge repositories)" {
		fmt.Printf("%s creating Release on %s based on Ref %s, tagging it as %s\n",
			promptui.IconGood,
			cBold.Sprint(repoSpec),
			cObject.Sprint(*opts.Ref),
			cObject.Sprint(*opts.TagName),
		)
	}
	release, _, err := GitlabClient.Releases.CreateRelease(
		repoSpec,
		&opts,
		nil,
	)
	if err != nil {
		return err
	}
	if resultRelease == "Git Tag" {
		fmt.Printf("%s created Release on %s based on Tag %s, Named %s\n",
			promptui.IconGood,
			cBold.Sprint(repoSpec),
			cObject.Sprint(release.TagName),
			cBold.Sprint(release.Name),
		)
	} else if resultRelease == "Git Commit (Slow on Huge repositories)" {
		fmt.Printf("%s created Release on %s based on Ref %s, Tagged %s, Named %s\n",
			promptui.IconGood,
			cBold.Sprint(repoSpec),
			cObject.Sprint(*opts.Ref),
			cObject.Sprint(release.TagName),
			cBold.Sprint(release.Name),
		)
	}

	return nil
}

func manageAssets(
	assetSlice []string,
	assetMap map[string]gitlab.ReleaseAssetLink,
) (
	[]string,
	map[string]gitlab.ReleaseAssetLink,
	error,
) {
	askAction := promptui.Select{
		Label: "pick an action",
		Items: []string{"continue", "create", "edit", "delete"},
	}

Continue:
	for {
		_, action, err := askAction.Run()
		if err != nil {
			return nil, nil, err
		}
		switch action {
		case "continue":
			break Continue
		case "create":
			newAsset, err := createAsset(nil, nil)
			if err == nil {
				assetMap[newAsset.Name] = *newAsset
				assetSlice = append(assetSlice, newAsset.Name)
			}
		case "edit":
			assetSlice, assetMap, err = editAsset(assetSlice, assetMap)
			if err != nil {
				fmt.Printf("%s couldn't edit asset due to %s\n", promptui.IconBad, err)
			}
		case "delete":
			assetSlice, assetMap, err = deleteAsset(assetSlice, assetMap)
			if err != nil {
				fmt.Printf("%s couldn't delete asset due to %s\n", promptui.IconBad, err)
			}
		}
	}
	return assetSlice, assetMap, nil
}

// Interactive way to delete an element from an slice and a map after questioning user for it
func deleteAsset(
	assetSlice []string,
	assetMap map[string]gitlab.ReleaseAssetLink,
) (
	[]string,
	map[string]gitlab.ReleaseAssetLink,
	error,
) {

	askAsset := promptui.Select{
		Label: "pick asset to delete",
		Items: assetSlice,
	}
	_, assetToDelete, err := askAsset.Run()
	if err != nil {
		return assetSlice, assetMap, err
	}

	// Delete asset from map
	delete(assetMap, assetToDelete)

	// Delete asset title from the assetSlice
	for index, title := range assetSlice {
		if title == assetToDelete {
			assetSlice[index] = assetSlice[len(assetSlice)-1] // Replace the removed element with our last element
			assetSlice = assetSlice[:len(assetSlice)-1]       // Remove the last element
			break
		}
	}

	return assetSlice, assetMap, nil
}

// Prompt the user for which asset to edit then trick him into thinking he is editing it by
// creating a new asset with defaults of the asset the user picked and then deleting the
// old one and replacing it with the new one
func editAsset(
	assetSlice []string,
	assetMap map[string]gitlab.ReleaseAssetLink,
) (
	[]string,
	map[string]gitlab.ReleaseAssetLink,
	error,
) {

	askAsset := promptui.Select{
		Label: "pick asset to edit",
		Items: assetSlice,
	}
	_, assetToEdit, err := askAsset.Run()
	if err != nil {
		return assetSlice, assetMap, err
	}

	// Store the old asset
	oldAsset := assetMap[assetToEdit]

	// Create a new asset, passing the old asset's Name and URL to give the illusion the user
	// is editing it
	newAsset, err := createAsset(&oldAsset.Name, &oldAsset.URL)
	if err != nil {
		return assetSlice, assetMap, err
	}

	// Delete the old asset from the map
	delete(assetMap, oldAsset.Name)

	// Add our asset
	assetMap[newAsset.Name] = *newAsset

	// Replace the oldAsset with the new asset
	for index, title := range assetSlice {
		if title == oldAsset.Name {
			assetSlice[index] = newAsset.Name
			break
		}
	}

	return assetSlice, assetMap, nil
}

// Interactive way to create a gitlab.ReleaseAssetLink and returning it to the user
// Pass nil, nil if you're creating a completely new asset, otherwise pass Name and
// Title to work as defaults.
func createAsset(Name *string, URL *string) (*gitlab.ReleaseAssetLink, error) {
	asset := gitlab.ReleaseAssetLink{}

	// Implement the default values into the asset
	if Name != nil {
		asset.Name = *Name
	}
	if URL != nil {
		asset.URL = *URL
	}

Continue:
	for {
		askAction := promptui.Select{
			Label: "pick attribute",
			Items: []string{"continue", "Name", "URL"},
		}
		_, action, err := askAction.Run()
		if err != nil {
			return nil, err
		}
		switch action {
		case "continue":
			break Continue
		case "Name":
			askName := promptui.Prompt{
				Label:   "Name of the asset",
				Default: asset.Name,
				Validate: func(s string) error {
					if s == "" {
						return errors.New("Path must not be empty")
					}
					return nil
				},
			}

			// Use the previous Name if the user is editing them again
			if asset.Name != "" {
				askName.Default = asset.Name
			}

			askNameResult, err := askName.Run()
			if err != nil {
				return nil, err
			}
			asset.Name = askNameResult

		case "URL":
			askURL := promptui.Prompt{
				Label:   "URL to the asset",
				Default: asset.URL,
				Validate: func(s string) error {
					if s == "" {
						return errors.New("Path must not be empty")
					}
					return err
				},
			}

			// Use the previous URL if the user is editing them again
			if asset.URL != "" {
				askURL.Default = asset.URL
			}

			askURLResult, err := askURL.Run()
			if err != nil {
				return nil, err
			}
			asset.URL = askURLResult
		}
	}

	// Check if both attributes were set as both are required
	if asset.Name == "" {
		return nil, errors.New("Failed to create asset, Name must be set")
	}
	if asset.URL == "" {
		return nil, errors.New("Failed to create asset, URL must be set")
	}

	return &asset, nil
}
