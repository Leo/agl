package mr

import (
	"fmt"
	"os"
	"strconv"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

var (
	// holds the work-in-progress marker we use, the default is Draft: but it accepts any
	// value and checks against the ones that are documented in the GitLab API
	wipMarker string

	// WIP marks a merge request as work-in-progress by prefixing the title with
	// Draft: (or a given valid work-in-progress marker) and updating it
	WIP = &cobra.Command{
		Use:   "wip <mrnum || URL>",
		Short: "mark a merge request as work-in-progress",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			// Check if merge request is actually num
			mrNum, err := strconv.Atoi(args[0])
			if err != nil {
				var repoSpec string
				repoSpec, mrNum, err = helpers.FindMergeRequestViaURL(args[0])
				if err != nil {
					fmt.Printf("%s %s\n", promptui.IconBad, err)
					os.Exit(1)
				}
				viper.Set("picked_repo", repoSpec)
			}
			if err := wipMergeRequest(mrNum); err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
		},
	}
)

func init() {
	WIP.Flags().BoolVarP(
		&dryRun,
		"dry-run",
		"n",
		false,
		"don't mark the merge request as work-in-progress, show which it would approve",
	)
	WIP.Flags().StringVarP(
		&wipMarker,
		"marker",
		"m",
		"Draft:",
		"Use this marker to denote a work-in-progress",
	)
}

func wipMergeRequest(mrNum int) error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}

	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	// Check if we have a valid work-in-progress marker, be more strict than GitLab. Only accept
	// case-sensitive matches of the GitLab API, while GitLab impelementation itself accepts any
	// case
	validMarker := false
	for _, keyword := range readyKeywords {
		if keyword == wipMarker {
			validMarker = true
		}
	}
	if !validMarker {
		return fmt.Errorf("given marker %s is not valid", cBold.Sprint(wipMarker))
	}

	if dryRun {
		fmt.Printf("Would mark %s as work-in-progress\n", cBold.Sprintf("!%d in %s", mrNum, repoSpec))
		return nil
	}
	mr, _, err := GitlabClient.MergeRequests.GetMergeRequest(
		repoSpec,
		mrNum,
		&gitlab.GetMergeRequestsOptions{},
		nil,
	)
	if err != nil {
		return err
	}

	// Check if the merge request is already a work-in-progress
	if v := isWIP(mr.Title); v != nil {
		return fmt.Errorf("merge request %s already marked as work-in-progress with %s",
			cBold.Sprintf("!%d in %s", mrNum, repoSpec),
			cBold.Sprint(*v),
		)
	}

	title := fmt.Sprintf("%s %s", wipMarker, mr.Title)

	fmt.Printf("%s marking %s as work-in-progress with %s\n",
		promptui.IconInitial,
		cBold.Sprintf("!%d in %s", mrNum, repoSpec),
		cBold.Sprint(wipMarker),
	)
	err = updateMR(repoSpec, mrNum, gitlab.UpdateMergeRequestOptions{Title: &title})
	if err != nil {
		return err
	}
	fmt.Printf("%s marked %s as work-in-progress with %s\n",
		promptui.IconGood,
		cBold.Sprintf("!%d in %s", mrNum, repoSpec),
		cBold.Sprint(wipMarker),
	)

	return nil

}
