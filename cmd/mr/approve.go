package mr

import (
	"fmt"
	"os"
	"strconv"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

var (
	// Approve approves a GitLab Merge Request
	Approve = &cobra.Command{
		Use:   "approve <mrnum || URL>",
		Short: "approves a merge request",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			// Check if merge request is actually num
			mrNum, err := strconv.Atoi(args[0])
			if err != nil {
				var repoSpec string
				repoSpec, mrNum, err = helpers.FindMergeRequestViaURL(args[0])
				if err != nil {
					fmt.Printf("%s %s\n", promptui.IconBad, err)
					os.Exit(1)
				}
				viper.Set("picked_repo", repoSpec)
			}
			if err := approveMergeRequest(mrNum); err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
		},
	}
)

func init() {
	Approve.Flags().BoolVarP(&dryRun, "dry-run", "n", false, "don't approve the merge request, show which it would approve")
}

func approveMergeRequest(mrNum int) error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}

	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	if dryRun {
		fmt.Printf("Would approve merge request !%s in %s\n",
			cBold.Sprint(mrNum),
			cBold.Sprint(repoSpec),
		)
		return nil
	}
	fmt.Printf("%s approving merge request !%s in %s\n",
		promptui.IconInitial,
		cBold.Sprint(mrNum),
		cBold.Sprint(repoSpec),
	)
	_, _, err = GitlabClient.MergeRequestApprovals.ApproveMergeRequest(
		repoSpec,
		mrNum,
		&gitlab.ApproveMergeRequestOptions{},
		nil,
	)
	if err != nil {
		return err
	}
	fmt.Printf("%s approved merge request !%s in %s\n",
		promptui.IconGood,
		cBold.Sprint(mrNum),
		cBold.Sprint(repoSpec),
	)
	return nil
}
