package mr

import (
	"fmt"
	"os"
	"strconv"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

// Reopen an merge request that was previously closed
var Reopen = &cobra.Command{
	Use:   "reopen <mrnum || URL>",
	Short: "reopen an merge request",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		// Check if merge request is actually num
		mrNum, err := strconv.Atoi(args[0])
		if err != nil {
			repoSpec, i, err := helpers.FindMergeRequestViaURL(args[0])
			if err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
			viper.Set("picked_repo", repoSpec)
			mrNum = i
		}
		if err := reopenMergeRequest(mrNum); err != nil {
			fmt.Printf("%s %s\n", promptui.IconBad, err)
			os.Exit(1)
		}
	},
}

func init() {
	Reopen.Flags().BoolVarP(&dryRun, "dry-run", "n", false, "don't close the merge request, show which it would close")
}

func reopenMergeRequest(mrNum int) error {
	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	state := "reopen"

	if dryRun {
		fmt.Printf("Would reopen merge request !%s in %s\n",
			cBold.Sprint(mrNum),
			cBold.Sprint(repoSpec),
		)
		return nil
	}
	fmt.Printf("%s reopening merge request !%s in %s\n",
		promptui.IconInitial,
		cBold.Sprint(mrNum),
		cBold.Sprint(repoSpec),
	)
	err = updateMR(repoSpec, mrNum, gitlab.UpdateMergeRequestOptions{StateEvent: &state})
	if err != nil {
		return err
	}
	fmt.Printf("%s reopened merge request !%s in %s\n",
		promptui.IconGood,
		cBold.Sprint(mrNum),
		cBold.Sprint(repoSpec),
	)
	return nil
}
