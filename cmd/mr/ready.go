package mr

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

var (
	// Ready marks a merge request as ready to be reviewed by removing the WIP:
	// from its title
	Ready = &cobra.Command{
		Use:   "ready <mrnum || URL>",
		Short: "mark a merge request as ready for review",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			// Check if merge request is actually num
			mrNum, err := strconv.Atoi(args[0])
			if err != nil {
				var repoSpec string
				repoSpec, mrNum, err = helpers.FindMergeRequestViaURL(args[0])
				if err != nil {
					fmt.Printf("%s %s\n", promptui.IconBad, err)
					os.Exit(1)
				}
				viper.Set("picked_repo", repoSpec)
			}
			if err := readyMergeRequest(mrNum); err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
		},
	}
)

func init() {
	Ready.Flags().BoolVarP(
		&dryRun,
		"dry-run",
		"n",
		false,
		"don't mark the merge request as ready, show which it would approve",
	)
}

func readyMergeRequest(mrNum int) error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}

	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	if dryRun {
		fmt.Printf("Would mark %s as ready\n", cBold.Sprintf("!%d in %s", mrNum, repoSpec))
		return nil
	}
	mr, _, err := GitlabClient.MergeRequests.GetMergeRequest(
		repoSpec,
		mrNum,
		&gitlab.GetMergeRequestsOptions{},
		nil,
	)
	if err != nil {
		return err
	}

	marker := isWIP(mr.Title)
	if marker == nil {
		return fmt.Errorf("merge request %s is not a work-in-progress", cBold.Sprintf("!%d in %s", mrNum, repoSpec))
	}

	// Remove the WIP: and any leading whitespace, it is very common for 'WIP: <title>' to be used
	// instead of 'WIP:<title>'
	title := strings.Replace(mr.Title, *marker, "", 1)
	title = strings.TrimLeft(title, "\t \n")

	fmt.Printf("%s marking %s as ready by removing %s\n",
		promptui.IconInitial,
		cBold.Sprintf("!%d in %s", mrNum, repoSpec),
		cBold.Sprint(*marker),
	)
	err = updateMR(repoSpec, mrNum, gitlab.UpdateMergeRequestOptions{Title: &title})
	if err != nil {
		return err
	}
	fmt.Printf("%s marked %s as ready by removing %s\n",
		promptui.IconGood,
		cBold.Sprintf("!%d in %s", mrNum, repoSpec),
		cBold.Sprint(*marker),
	)

	return nil

}
