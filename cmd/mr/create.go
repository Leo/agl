package mr

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"sync"

	git "github.com/libgit2/git2go/v31"
	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/constants"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

var (
	mrCreateEdit     bool
	mrCreateOrigin   string
	mrCreateUpstream string
	mrCreateSource   string
	mrCreateTarget   string
	mrCreateLabels   string

	waitGroup sync.WaitGroup

	// Create a merge request
	Create = &cobra.Command{
		Use:     "create",
		Aliases: []string{"mk"},
		Short:   "Create Merge Request",
		Long: `Create Merge Request based on the current branch of the repo, it is necessary

that both the source repository and the upstream repository be present in the form of remotes,
the source repository remote is commonly called origin and is the repo where you have
permissions push, the upstream repository remote is commonly called upstream and is the repo
where changes you've made are merged`,
		Args: cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			// run createMergeRequest
			if err := createMergeRequest(); err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
		},
	}
)

func init() {
	Create.Flags().BoolVarP(&dryRun, "dry-run", "n", false, "don't create merge request, show attributes")
	Create.Flags().BoolVarP(
		&mrCreateEdit,
		"edit",
		"e",
		false,
		"Prompt user to edit attributes like title, description and labels",
	)
	Create.Flags().StringVarP(
		&mrCreateOrigin,
		"origin",
		"o",
		"origin",
		"remote potinting source repository",
	)
	Create.Flags().StringVarP(
		&mrCreateUpstream,
		"upstream",
		"u",
		"upstream",
		"remote pointing to upstream repository",
	)
	Create.Flags().StringVarP(
		&mrCreateSource,
		"source",
		"s",
		"",
		"branch that has changes to be merged (defaults to currently checked out branch)",
	)
	Create.Flags().StringVarP(
		&mrCreateTarget,
		"target",
		"t",
		"",
		"branch where changes will be merged into",
	)
	Create.Flags().StringVarP(
		&mrCreateLabels,
		"labels",
		"l",
		"",
		"labels to set when creating the merge request",
	)
}

func removeDuplicates(elements []string) []string {
	// Use map to record duplicates as we find them.
	encountered := map[string]bool{}
	result := []string{}

	for v := range elements {
		if encountered[elements[v]] == true {
			// Do not add duplicate.
		} else {
			// Record this element as an encountered element.
			encountered[elements[v]] = true
			// Append to result slice.
			result = append(result, elements[v])
		}
	}
	// Return the new slice.
	return result
}

func requestRepoPid(GitlabClient *gitlab.Client, projectPath string, channel chan *int) {
	defer waitGroup.Done() // Schedule call to the end of this function or whenever we return

	res, _, err := GitlabClient.Projects.GetProject(
		projectPath,
		&gitlab.GetProjectOptions{},
		nil,
	)
	if err != nil {
		fmt.Printf("%s  couldn't get URL of upstream repo due to %s\n", promptui.IconWarn, err)
		channel <- nil
		return
	}
	channel <- &res.ID
}

func createMergeRequest() error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}

	// Open the repository that we are currently in, we use it to find remotes
	r, err := git.OpenRepository(".") // PWD
	if err != nil {
		return err
	}

	// Get the origin remote by the value passed ot us
	originRemote, err := r.Remotes.Lookup(mrCreateOrigin)
	if err != nil {
		return err
	}
	originURL := originRemote.Url()                                   // URL of the origin remote
	originRepoSpec, err := helpers.GetNamespacePathFromURL(originURL) // NAMESPACE/PATH of origin
	if err != nil {
		return err
	}

	// Get the upstream remote by the value passed ot us
	upstreamRemote, err := r.Remotes.Lookup(mrCreateUpstream)
	if err != nil {
		return err
	}
	upstreamURL := upstreamRemote.Url()                                   // URL of the upstream remote
	upstreamRepoSpec, err := helpers.GetNamespacePathFromURL(upstreamURL) // NAMESPACE/PATH of upstream
	if err != nil {
		return err
	}

	// Get all the local branches, let the user pick one via a prompt
	localBranches, err := r.NewBranchIterator(git.BranchLocal)
	if err != nil {
		return err
	}

	allSourceBranches := make(map[string]bool)

	err = localBranches.ForEach(func(b *git.Branch, _ git.BranchType) error {
		allSourceBranches[b.Shorthand()] = true
		return nil
	})

	// Get the current checked out branch, our default
	headRef, err := r.Head()
	if err != nil {
		return err
	}

	sourceBranch := mrCreateSource
	if sourceBranch == "" {
		sourceBranch = headRef.Branch().Shorthand()
	}

	if !allSourceBranches[sourceBranch] {
		return fmt.Errorf("branch %s is not present locally", sourceBranch)
	}

	// Create goroutine and get the Project ID from the repo
	targetRepoID := make(chan *int)
	waitGroup.Add(1)
	go requestRepoPid(GitlabClient, upstreamRepoSpec, targetRepoID)

	var upstreamBranch string

	// --upstream wasn't passed, check if we are in aports and set an appropriate
	// branch or set the default branch master
	if mrCreateTarget == "" {
		if upstreamRepoSpec == "alpine/aports" && strings.Contains(helpers.ProfileGet("host"), "gitlab.alpinelinux.org") {
			upstreamBranch = helpers.AlpinePrefix(sourceBranch)

		} else {
			upstreamBranch = "master"
		}
	} else {
		upstreamBranch = mrCreateTarget
	}

	// Switch to branch
	headRef, err = helpers.SwitchToBranch(r, sourceBranch)
	if err != nil {
		return err
	}

	// Fetch it so we have an updated view for the rebase
	fmt.Printf("%s fetching %s/%s\n",
		promptui.IconInitial,
		upstreamRemote.Name(),
		upstreamBranch,
	)
	err = upstreamRemote.Fetch(
		[]string{"refs/head" + upstreamBranch},
		&git.FetchOptions{
			RemoteCallbacks: git.RemoteCallbacks{
				CredentialsCallback: helpers.GitCredentials,
			},
		},
		"",
	)
	fmt.Printf("%s fetched %s/%s\n",
		promptui.IconGood,
		upstreamRemote.Name(),
		upstreamBranch,
	)

	// Rebase your local branch onto the target branch of the remote
	fmt.Printf("%s rebasing %s onto %s/%s\n",
		promptui.IconInitial,
		sourceBranch,
		upstreamRemote.Name(),
		upstreamBranch,
	)
	rebase, err := helpers.PerformRebaseOnto(
		r,
		fmt.Sprintf("%s/%s", upstreamRemote.Name(), upstreamBranch),
		sourceBranch,
	)
	if err != nil {
		fmt.Printf("%s failed rebase of %s onto %s/%s: %s\n",
			promptui.IconBad,
			sourceBranch,
			upstreamRemote.Name(),
			upstreamBranch,
			err,
		)
		origErr := err
		// PerformRebaseOnto returns nil if the rebase didn't start yet
		if rebase != nil {
			err = rebase.Abort()
			if err != nil {
				return fmt.Errorf("%s, %s", origErr, err)
			}
		}
		return origErr
	}
	err = rebase.Finish()
	if err != nil {
		return err
	}
	fmt.Printf("%s rebased %s onto %s/%s\n",
		promptui.IconGood,
		sourceBranch,
		upstreamRemote.Name(),
		upstreamBranch,
	)

	// TODO: replace this with libgit2/git2go way once helpers.GitCredentials works with SSH
	fmt.Printf("%s pushing %s to %s\n",
		promptui.IconInitial,
		sourceBranch,
		originRepoSpec,
	)
	gitExecutable, err := exec.LookPath("git")
	if err != nil {
		return err
	}
	// Equivalent to `git push remote refs/heads/branch`
	cmd := exec.Command(gitExecutable, "push", originRemote.Name(), "--force", "refs/heads/"+sourceBranch)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err = cmd.Run()
	if err != nil {
		if stderr.Len() == 0 {
			return err
		}
		return fmt.Errorf("%s: %s", err, stderr.String())
	}

	// Uncomment this once the TODO above is fixed
	// err = sourceRef.Push(
	// 	[]string{"refs/heads/" + sourceBranch},
	// 	&git.PushOptions{
	// 		RemoteCallbacks: git.RemoteCallbacks{
	// 			CredentialsCallback:      helpers.GitCredentials,
	// 			CertificateCheckCallback: helpers.GitCertificates,
	// 		},
	// 	},
	// )
	// if err != nil {
	// 	return err
	// }

	fmt.Printf("%s pushed %s to %s\n",
		promptui.IconGood,
		sourceBranch,
		originRepoSpec,
	)

	// Read IID into a variable first so it blocks until the function is done
	// It won't block if it can already read targetRepoID
	IID := <-targetRepoID
	if IID == nil {
		return fmt.Errorf("couldn't acquire ProjectID for %s", upstreamRepoSpec)
	}

	upstreamBranchRef, err := r.References.Lookup("refs/remotes/" + upstreamRemote.Name() + "/" + upstreamBranch)
	if err != nil {
		return err
	}

	upstreamCommit, err := r.LookupCommit(upstreamBranchRef.Branch().Target())
	if err != nil {
		return err
	}

	askCommits, commitMap, err := helpers.GitCommitIterateUntil(r, upstreamCommit.Id())
	if err != nil {
		return err
	} else if len(commitMap) == 0 {
		return fmt.Errorf("There are no commits to be merged between %s/%s and %s/%s",
			originRemote.Name(),
			sourceBranch,
			upstreamRemote.Name(),
			upstreamBranch,
		)
	}

	var defCommit git.Commit

	if len(commitMap) == 1 {
		// There is only one commit, pick it as default
		defCommit = commitMap[askCommits[0]]
	}

	if len(commitMap) > 1 {
		// There are more than one commit, prompt the user to select the commit, take it 10 by 10
		askCommit := promptui.Select{
			Label: "pick commit",
			Items: askCommits,
			Size:  10,
		}

		_, askCommitResult, err := askCommit.Run()
		if err != nil {
			return err
		}

		defCommit = commitMap[askCommitResult]
	}

	commitMsg := strings.Split(defCommit.Message(), "\n")

	// Somehow one can't initialize a variable already as a pointer even for something as trivial
	// as this
	trueBool := true

	opts := gitlab.CreateMergeRequestOptions{
		Title:              &commitMsg[0],
		SourceBranch:       &sourceBranch,
		TargetBranch:       &upstreamBranch,
		TargetProjectID:    IID,
		RemoveSourceBranch: &trueBool,
	}

	if len(commitMsg) > 1 {
		t := strings.Join(commitMsg[2:], "\n")
		opts.Description = &t
	}

	var finalLabels string
	if mrCreateLabels != "" {
		finalLabels = mrCreateLabels
	}

	/*
	 * TODO: Handle every single option through the usage of a select prompt that
	 * allows users to pick an attribute and an specific prompt for each type of
	 * attribute (example selection for a boolean or multiple-choice, and a prompt
	 * for ones that require an attribute)
	 *
	 * Implemented (Either here or elsewhere in Code due to be obligatory):
	 *  - Title (Required, asked separately)
	 *  - SourceBranch (Required, handled separately)
	 *  - TargetBranch (Required, handled separately)
	 *  - TargetProjectID (Required, deduced)
	 *  - RemoveSourceBranch (No reason not to do it, so always true)
	 *  - Description
	 *  - Labels
	 * Rejected Implementations:
	 *  - AssigneeID(s) (Requires Int no way to use Username)
	 *  - MilestoneID (Requires Int, no way to use Name)
	 *
	 * Reference: https://godoc.org/github.com/xanzy/go-gitlab#CreateMergeRequestOptions
	 * Reference: https://docs.gitlab.com/ce/api/merge_requests.html#create-mr
	 */
	if mrCreateEdit {
	Continue:
		for {
			askAttribute := promptui.Select{
				Label: "Pick an attribute of the Project to edit or continue",
				Items: []string{"continue", "title", "description", "labels"}, // Split this away into its own variable
			}
			_, askResult, err := askAttribute.Run()
			if err != nil {
				fmt.Println(err)
				break Continue
			}
			switch askResult {
			case "title":
				askTitle := promptui.Prompt{
					Label:   "Title",
					Default: *opts.Title,
				}
				commitTitle, err := askTitle.Run()
				if err != nil {
					return err
				}
				opts.Title = &commitTitle
			case "description":
				resultDescription, err := constants.EditWithEditor(opts.Description)
				if err != nil {
					fmt.Printf("%s failed to get description due to %s\n", promptui.IconBad, err)
					continue
				} else if resultDescription == nil {
					fmt.Printf("%s description is empty, not changing\n", promptui.IconWarn)
					continue
				}
				resultDesc := strings.TrimSpace(*resultDescription)
				opts.Description = &resultDesc
			case "labels":
				askLabels := promptui.Prompt{
					Label:   "comma-separated list of labels",
					Default: finalLabels,
				}
				resultLabels, err := askLabels.Run()
				if err != nil {
					fmt.Printf("%s failed to get labels due to %s\n", promptui.IconBad, err)
					continue
				}
				finalLabels = resultLabels
			case "continue":
				break Continue
			}
		}
	}

	// Apply the labels
	if finalLabels != "" {
		opts.Labels = gitlab.Labels{finalLabels}
	}

	if dryRun {
		// Show information about what we would have done
		fmt.Printf("Title: %s\n", *opts.Title)
		if opts.Description != nil {
			for _, line := range strings.Split(*opts.Description, "\n") {
				fmt.Printf("Description: %s\n", line)
			}
		}
		fmt.Printf("Source Repo: %s\nTarget Repo: %s (%d)\nSource Branch: %s\nTarget Branch: %s\n",
			originRepoSpec,
			upstreamRepoSpec,
			*IID,
			sourceBranch,
			upstreamBranch,
		)
		if opts.Description != nil {
			for _, line := range strings.Split(*opts.Description, "\n") {
				fmt.Printf("Description %s\n", line)
			}
		}
		if len(opts.Labels) > 0 {
			for _, label := range opts.Labels {
				if label == "" {
					continue
				}
				fmt.Printf("Label: %s\n", label)
			}
		}
		return nil
	}
	fmt.Printf("%s creating Merge Request on %s\n",
		promptui.IconInitial,
		upstreamRepoSpec,
	)
	mr, _, err := GitlabClient.MergeRequests.CreateMergeRequest(
		originRepoSpec,
		&opts,
	)
	if err != nil {
		return err
	}
	fmt.Printf("%s created !%d on %s, URL: %s\n",
		promptui.IconGood,
		mr.IID,
		upstreamRepoSpec,
		mr.WebURL)

	/*
	 * TODO: write the branch name into cache along with the merge request it refers to like in mgmr
	 * 		 or use more API queries to find which branch maps to which MR
	 */
	return nil
}
