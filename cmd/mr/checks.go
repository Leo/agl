package mr

import (
	"fmt"
	"os"
	"sort"
	"strconv"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

var (
	// Checks show all the pipelines from a merge request, their status and whether
	// their state is 'success' (in green), 'failed' (in red) or 'skipped' (in yellow)
	Checks = &cobra.Command{
		Use:   "checks <mrnum || URL>",
		Short: "shows status of pipelines of a merge request",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			// Check if merge request is actually num
			mrNum, err := strconv.Atoi(args[0])
			if err != nil {
				var repoSpec string
				repoSpec, mrNum, err = helpers.FindMergeRequestViaURL(args[0])
				if err != nil {
					fmt.Printf("%s %s\n", promptui.IconBad, err)
					os.Exit(1)
				}
				viper.Set("picked_repo", repoSpec)
			}
			if err := showMergeRequestPipelines(mrNum); err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
		},
	}
)

func init() {
	Checks.Flags().BoolVarP(
		&dryRun,
		"dry-run",
		"n",
		false,
		"show from which merge request it would show the pipelines",
	)
}

func showMergeRequestPipelines(mrNum int) error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}

	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	if dryRun {
		fmt.Printf("Would show pipelines from merge request !%s in %s\n",
			cBold.Sprint(mrNum),
			cBold.Sprint(repoSpec),
		)
		return nil
	}
	pipelines, _, err := GitlabClient.MergeRequests.ListMergeRequestPipelines(
		repoSpec,
		mrNum,
		nil,
	)
	if err != nil {
		return err
	}

	// Holds a slice of all pipelines by id, we sort this to show all pipelines from
	// the one with the highest ID to the one with the lowest ID
	var pipelineSlice []int

	// Holds a relation between an ID and a pipeline
	pipelineMap := make(map[int]*gitlab.PipelineInfo)

	for _, pipeline := range pipelines {
		pipelineSlice = append(pipelineSlice, pipeline.ID) // Populate with the ID
		pipelineMap[pipeline.ID] = pipeline                // Populate with the IDs referring a pipeline
	}

	if len(pipelineSlice) < 1 {
		return fmt.Errorf("!%d has no pipelines", mrNum)
	}

	// Sort the Ints in reverse order
	sort.Sort(sort.Reverse(sort.IntSlice(pipelineSlice)))

	for _, pipelineID := range pipelineSlice {
		if _, ok := pipelineMap[pipelineID]; !ok {
			continue // We hit a pipeline that is not present in the map, skip showing it
		}

		switch pipelineMap[pipelineID].Status {
		case "success":
			fmt.Printf("%s ",
				cPositive.Sprint("*"),
			)
		case "skipped":
			fmt.Printf("%s ",
				cWarn.Sprint("*"),
			)
		case "failed":
			fmt.Printf("%s ",
				cNegative.Sprint("*"),
			)
		}

		fmt.Printf("id(%s) sha(%s) created(%s)",
			cBold.Sprint(pipelineMap[pipelineID].ID),
			cObject.Sprint(pipelineMap[pipelineID].SHA),
			cTime.Sprint(pipelineMap[pipelineID].CreatedAt.Format("2006-01-02 15:04 -0700")),
		)

		if pipelineMap[pipelineID].UpdatedAt.After(*pipelineMap[pipelineID].CreatedAt) {
			fmt.Printf(" updated(%s)",
				cTime.Sprint(pipelineMap[pipelineID].UpdatedAt.Format("2006-01-02 15:04 -0700")),
			)
		}
		fmt.Printf("\n")
	}

	return nil
}
