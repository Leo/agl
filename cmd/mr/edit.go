package mr

import (
	"errors"
	"fmt"
	"os"
	"strconv"

	"github.com/google/go-cmp/cmp"
	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/constants"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

var (
	editTitle              string
	editDescription        bool
	editTargetBranch       string
	editLabels             string
	editAddLabels          string
	editRemoveLabels       string
	editStateEvent         string
	editRemoveSourceBranch string
	editSquash             string
	editDiscussionLocked   string
	editAllowCollaboration string

	// Edit an existing issue
	Edit = &cobra.Command{
		Use:   "edit <mrnum || URL>",
		Short: "edit attributes of a merge request",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			// Check if issue is actually num
			mrNum, err := strconv.Atoi(args[0])
			if err != nil {
				var repoSpec string
				repoSpec, mrNum, err = helpers.FindMergeRequestViaURL(args[0])
				if err != nil {
					fmt.Printf("%s %s\n", promptui.IconBad, err)
					os.Exit(1)
				}
				// Modify the global variable repo, it can cause confusion but this
				// is the global variable that the user can set via --path, but if
				// the user is passing the full URL then there is no reason to respect
				// it
				viper.Set("picked_repo", repoSpec)
			}
			if err := editMR(mrNum); err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
		},
	}
)

func init() {
	Edit.Flags().StringVarP(&editTitle,
		"title",
		"t",
		"",
		"set title",
	)

	Edit.Flags().BoolVarP(&editDescription,
		"description",
		"d",
		false,
		"set description via $EDITOR",
	)
	Edit.Flags().StringVar(&editTargetBranch,
		"target-branch",
		"",
		"set target branch",
	)
	Edit.Flags().StringVarP(&editLabels,
		"labels",
		"l",
		"",
		"comma-separated list of labels to set",
	)
	Edit.Flags().StringVar(&editAddLabels,
		"add-labels",
		"",
		"comma-separated list of labels to add",
	)
	Edit.Flags().StringVar(&editRemoveLabels,
		"remove-labels",
		"",
		"comma-separated list of labels to remove",
	)
	Edit.Flags().StringVarP(&editStateEvent,
		"state",
		"s",
		"",
		"set state: {open|closed}",
	)
	Edit.Flags().StringVar(&editRemoveSourceBranch,
		"remove-source-branch",
		"",
		"indicate the merge request should remove the source branch when merging : {yes|no}",
	)
	Edit.Flags().StringVar(&editSquash,
		"squash",
		"",
		"squash commits into a single commit when merging : {yes|no}",
	)
	Edit.Flags().StringVar(&editDiscussionLocked,
		"discussion",
		"",
		"set discussion permission: {locked|unlocked}",
	)
	Edit.Flags().StringVar(&editAllowCollaboration,
		"allow-collaboration",
		"",
		"allow commits from members who can merge to the target branch: {yes|no}",
	)
}

func editMR(mrNum int) error {
	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	opts := gitlab.UpdateMergeRequestOptions{}

	if editTitle != "" {
		opts.Title = &editTitle
	}

	if editDescription {
		desc, err := constants.EditWithEditor(nil)
		if err != nil {
			fmt.Printf("%s failed to get description due to %s\n", promptui.IconBad, err)
		} else if desc == nil && *desc == "" {
			fmt.Printf("%s description must not be empty\n", promptui.IconBad)
		} else {
			opts.Description = desc
		}
	}

	if editTargetBranch != "" {
		opts.TargetBranch = &editTargetBranch
	}

	if editLabels != "" && editAddLabels != "" {
		return errors.New("--labels and --add-labels are mutually exclusive, add the values of --add-labels to --labels")
	}
	if editLabels != "" && editRemoveLabels != "" {
		return errors.New("--remove-labels with --labels is redundant, --labels will remove all labels not in it")
	}

	if editLabels != "" {
		opts.Labels = gitlab.Labels{editLabels}
	}

	if editAddLabels != "" {
		opts.AddLabels = gitlab.Labels{editAddLabels}
	}

	if editRemoveLabels != "" {
		opts.RemoveLabels = gitlab.Labels{editRemoveLabels}
	}

	switch editStateEvent {
	case "":
		// No value was given to us, continue as normal
		break
	case "open":
		t := "reopen"
		opts.StateEvent = &t
	case "closed":
		t := "close"
		opts.StateEvent = &t
	default:
		fmt.Printf("%s --state must be open|closed, got %s\n", promptui.IconBad, editStateEvent)
	}

	switch editRemoveSourceBranch {
	case "":
		// No value was given to us, continue as normal
		break
	case "yes":
		t := true
		opts.RemoveSourceBranch = &t
	case "no":
		t := false
		opts.RemoveSourceBranch = &t
	default:
		fmt.Printf("%s --remove-source-branch must be yes|no, got %s\n", promptui.IconBad, editRemoveSourceBranch)
	}

	switch editSquash {
	case "":
		// No value was given to us, continue as normal
		break
	case "yes":
		t := true
		opts.Squash = &t
	case "no":
		t := false
		opts.Squash = &t
	default:
		fmt.Printf("%s --squash must be yes|no, got %s\n", promptui.IconBad, editSquash)
	}

	switch editDiscussionLocked {
	case "":
		// No value was given to us, continue as normal
		break
	case "locked":
		t := true
		opts.DiscussionLocked = &t
	case "unlocked":
		t := false
		opts.DiscussionLocked = &t
	default:
		fmt.Printf("%s --discussion must be locked|unlocked, got %s\n", promptui.IconBad, editDiscussionLocked)
	}

	switch editAllowCollaboration {
	case "":
		// No value was given to us, continue as normal
		break
	case "locked":
		t := true
		opts.AllowCollaboration = &t
	case "unlocked":
		t := false
		opts.AllowCollaboration = &t
	default:
		fmt.Printf("%s --allow-collbatoration must be locked|unlocked, got %s\n", promptui.IconBad, editAllowCollaboration)
	}

	// Check if we were given any parameters
	if cmp.Equal(opts, gitlab.UpdateMergeRequestOptions{}) {
		return errors.New("no new values were given to us")
	}

	// Update issue with our changes
	fmt.Printf("%s updating !%s from %s\n", promptui.IconInitial, cBold.Sprint(mrNum), repoSpec)
	err = updateMR(repoSpec, mrNum, opts)
	if err != nil {
		return err
	}
	fmt.Printf("%s updated !%s from %s\n", promptui.IconGood, cBold.Sprint(mrNum), repoSpec)

	return nil
}

func updateMR(repoSpec string, mrNum int, opts gitlab.UpdateMergeRequestOptions) error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}

	_, _, err = GitlabClient.MergeRequests.UpdateMergeRequest(
		repoSpec,
		mrNum,
		&opts,
		nil,
	)
	if err != nil {
		return err
	}
	return nil
}
