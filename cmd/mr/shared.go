package mr

import (
	"regexp"

	"gitlab.alpinelinux.org/Leo/agl/constants"
)

var (
	dryRun bool

	cBold     = constants.ColorBold
	cUser     = constants.ColorUser
	cObject   = constants.ColorObject
	cPositive = constants.ColorPositive
	cNegative = constants.ColorNegative
	cWarn     = constants.ColorWarn
	cTime     = constants.ColorTime

	// GitLab keywords for a merge request that is ready, they are all translated in lowercase
	// in the code for checking purposes.
	// https://docs.gitlab.com/ee/user/project/merge_requests/work_in_progress_merge_requests.html
	// WIP: is deprecated in favour of Draft but it is much better named so lets support it as long
	// as our recognized hosts don't switch over to 14.0, also escape all the control caracthers
	// as we use this in a regex
	readyKeywords = []string{"\\[WIP\\]", "WIP:", "\\[Draft\\]", "Draft:", "\\(Draft\\)"}
)

// isWIP takes a string and matches it case-insensitive against every known GitLab keywords for a
// merge request that is a work-in-progress, if there is a match it returns a pointer to the string
// that was matched, otherwise it returns nil
func isWIP(title string) *string {
	for _, element := range readyKeywords {
		re := regexp.MustCompile("^(?i)" + element)
		if matched := re.FindString(title); matched != "" {
			return &matched
		}
	}
	return nil
}
