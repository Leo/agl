package mr

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

var (
	showDescription bool

	// View shows information of a merge request
	View = &cobra.Command{
		Use:   "view <mrnum || URL>",
		Short: "view attributes of a merge request",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			// Check if merge request is actually num
			mrNum, err := strconv.Atoi(args[0])
			if err != nil {
				var repoSpec string
				repoSpec, mrNum, err = helpers.FindMergeRequestViaURL(args[0])
				if err != nil {
					fmt.Printf("%s %s\n", promptui.IconBad, err)
					os.Exit(1)
				}
				viper.Set("picked_repo", repoSpec)
			}
			if err := viewMergeRequest(mrNum); err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
		},
	}
)

func init() {
	View.Flags().BoolVarP(
		&dryRun,
		"dry-run",
		"n",
		false,
		"show which merge request it would view",
	)
	View.Flags().BoolVarP(
		&showDescription,
		"description",
		"d",
		false,
		"show description of the merge request",
	)
}

func viewMergeRequest(mrNum int) error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}

	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	t := true

	if dryRun {
		fmt.Printf("Would view merge request !%s in %s\n",
			cBold.Sprint(mrNum),
			cBold.Sprint(repoSpec),
		)
		return nil
	}
	mr, _, err := GitlabClient.MergeRequests.GetMergeRequest(
		repoSpec,
		mrNum,
		&gitlab.GetMergeRequestsOptions{
			IncludeDivergedCommitsCount: &t,
		},
		nil,
	)
	if err != nil {
		return err
	}

	// TODO: This might be better the same as all the others, * at the start
	fmt.Printf("Merge Request !%d at %s\n", mr.IID, mr.WebURL)

	fmt.Printf("* %s\n", mr.Title)

	fmt.Printf("* targets %s\n", cObject.Sprint(mr.TargetBranch))

	switch mr.State {
	case "opened":
		fmt.Printf("%s is %s\n", cPositive.Sprint("*"), cPositive.Sprint("open"))
	case "merged":
		fmt.Printf("%s was %s by %s on %s at %s\n",
			cPositive.Sprint("*"),
			cPositive.Sprint("merged"),
			cUser.Sprint(mr.MergedBy.Username),
			cTime.Sprint(mr.MergedAt.Format("Monday 2 January 2006")),
			cTime.Sprint(mr.MergedAt.Format("15:04:05")),
		)
	case "closed":
		fmt.Printf("%s was %s by %s on %s at %s\n",
			cNegative.Sprint("*"),
			cNegative.Sprint("closed"),
			cUser.Sprint(mr.ClosedBy.Username),
			cTime.Sprint(mr.ClosedAt.Format("Monday 2 January 2006")),
			cTime.Sprint(mr.ClosedAt.Format("15:04:05")),
		)
	}

	// TODO: Possibly integrate this into future checks for mr.MergeStatus
	if mr.State == "opened" && mr.DivergedCommitsCount > 0 {
		fmt.Printf("%s requires %s before merging\n",
			cWarn.Sprint("*"),
			cWarn.Sprint("rebase"),
		)
	}

	// No reason to show this if it isn't open
	if mr.User.CanMerge && mr.State == "opened" {
		fmt.Printf("%s you have %s to merge it\n",
			cPositive.Sprint("*"),
			cPositive.Sprint("permissions"),
		)
	}

	switch mr.Pipeline.Status {
	case "success":
		fmt.Printf("%s has %s pipeline\n",
			cPositive.Sprint("*"),
			cPositive.Sprint("passed"),
		)
	case "pending", "canceled":
		fmt.Printf("%s has %s pipeline\n",
			cWarn.Sprint("*"),
			cWarn.Sprint(mr.Pipeline.Status),
		)
	case "failed":
		fmt.Printf("%s has %s pipeline\n",
			cNegative.Sprint("*"),
			cNegative.Sprint("failed"),
		)
	case "running":
		fmt.Printf("%s has %s pipeline\n",
			cObject.Sprint("*"),
			cObject.Sprint("running"),
		)
	}

	if mr.MergeWhenPipelineSucceeds {
		fmt.Printf("%s will be merged when pipeline %s\n",
			cPositive.Sprint("*"),
			cPositive.Sprint("succeeds"),
		)
	}

	fmt.Printf("* created by %s on %s at %s\n",
		cUser.Sprint(mr.Author.Username),
		cTime.Sprint(mr.CreatedAt.Format("Monday 2 January 2006")),
		cTime.Sprint(mr.CreatedAt.Format("15:04:05")),
	)

	if !mr.CreatedAt.Equal(*mr.UpdatedAt) {
		fmt.Printf("* last updated on %s at %s\n",
			cTime.Sprint(mr.UpdatedAt.Format("Monday 2 January 2006")),
			cTime.Sprint(mr.UpdatedAt.Format("15:04:05")),
		)
	}

	if mr.Milestone != nil {
		fmt.Printf("* part of the %s milestone\n", cObject.Sprintf(mr.Milestone.Title))
	}

	if mr.WorkInProgress {
		fmt.Printf("%s is a %s\n", cWarn.Sprint("*"), cWarn.Sprint("work in progress"))
	}

	if mr.Assignees != nil && len(mr.Assignees) > 0 {
		fmt.Print("* assigned to:\n")
		for _, assignee := range mr.Assignees {
			fmt.Printf("  %s\n", cUser.Sprint(assignee.Username))
		}
	} else if mr.Assignee != nil {
		fmt.Printf("* assigned to: %s\n", cUser.Sprint(mr.Assignee.Username))
	}

	if len(mr.Labels) > 0 {
		fmt.Printf("* has the labels:\n")
		for _, label := range mr.Labels {
			fmt.Printf("  %s\n", cObject.Sprintf(label))
		}
	}

	if mr.Upvotes > 0 {
		fmt.Printf("%s has %s\n",
			cPositive.Sprint("*"),
			cPositive.Sprintf("%d upvotes", mr.Upvotes),
		)
	}

	if mr.Downvotes > 0 {
		fmt.Printf("%s has %s\n",
			cNegative.Sprint("*"),
			cNegative.Sprintf("%d downvotes", mr.Downvotes),
		)
	}

	if mr.UserNotesCount == 1 {
		fmt.Printf("* there is %s comment\n", cBold.Render(mr.UserNotesCount))
	} else if mr.UserNotesCount > 1 {
		fmt.Printf("* there are %s comments\n", cBold.Render(mr.UserNotesCount))
	}

	if mr.DiscussionLocked {
		fmt.Printf("%s discussion is %s\n", cNegative.Sprint("*"), cNegative.Sprint("locked"))
	}

	if mr.Subscribed && helpers.ProfileGet("username") != mr.Author.Username {
		fmt.Printf("%s you're %s to this merge request\n", cPositive.Sprint("*"), cPositive.Sprint("subscribed"))
	}

	// It is imperative that this go at the end because it can be a massive wall of text
	if mr.Description != "" && showDescription {
		fmt.Print("* Description:\n")
		for _, line := range strings.Split(mr.Description, "\n") {
			fmt.Printf("  %s\n", line)
		}
	}

	return nil
}
