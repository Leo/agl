package mr

import (
	"errors"
	"fmt"
	"os"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
	"golang.org/x/crypto/ssh/terminal"
)

var (
	listState     string
	listOrderBy   string
	listSort      string
	listLabel     string
	listNotLabel  string
	listMilestone string
	listScope     string
	listSource    string
	listTarget    string
	listSearch    string
	listWIP       string
	listLimit     int

	// List (or search) for merge requests in a repository
	List = &cobra.Command{
		Use:     "list",
		Aliases: []string{"search"},
		Short:   "list and filter merge requests",
		Args:    cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			if err := listMergeRequests(); err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
		},
	}
)

func init() {
	List.Flags().StringVarP(&listState,
		"state",
		"s",
		"",
		"Filter by state: {open|closed|locked|merged|all}",
	)
	List.Flags().StringVar(&listSort,
		"sort",
		"desc",
		"Sort in descending (or desc) or ascending (or asc) order",
	)
	List.Flags().StringVarP(&listMilestone,
		"milestone",
		"m",
		"",
		"Filter by the milestone title, None for no milestones, Any for any milestone",
	)
	List.Flags().StringVarP(&listLabel,
		"label",
		"l",
		"",
		"Filter by one or more (comma separated) labels",
	)
	List.Flags().StringVar(&listNotLabel,
		"not-label",
		"",
		"Filter by absence of one or more (comma separated) labels",
	)
	List.Flags().StringVarP(&listScope,
		"scope",
		"S",
		"all",
		"Filter by scope: {created-by-me|assigned-to-me|all}",
	)
	List.Flags().StringVar(&listSource,
		"source",
		"",
		"Filter by the the source branch",
	)
	List.Flags().StringVarP(&listTarget,
		"target",
		"t",
		"",
		"Filter by the the target branch",
	)
	List.Flags().StringVar(&listSearch,
		"search",
		"",
		"Search string against the title and/or description",
	)
	List.Flags().StringVarP(&listWIP,
		"wip",
		"w",
		"",
		"Filter by whether it is a Work in Progress: {yes|no}",
	)
	List.Flags().StringVarP(&listOrderBy,
		"order-by",
		"o",
		"created",
		"Order by whenever it was created or last updated: {created|updated}",
	)
	List.Flags().IntVarP(&listLimit,
		"limit",
		"L",
		20,
		"Maximum number of Merge Requests to fetch",
	)
}

func listMergeRequests() error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}

	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	filters := gitlab.ListProjectMergeRequestsOptions{}

	switch listState {
	case "open", "":
		t := "opened"
		filters.State = &t
	case "closed", "locked", "merged", "all":
		filters.State = &listState
	default:
		return fmt.Errorf("%s is an invalid state to filter", listState)
	}

	switch listOrderBy {
	case "created", "":
		t := "created_at"
		filters.OrderBy = &t
	case "updated":
		t := "updated_at"
		filters.OrderBy = &t
	default:
		return fmt.Errorf("%s is an invalid attribute to order by", listState)
	}

	switch listSort {
	case "ascending":
		t := "asc"
		filters.Sort = &t
	case "descending", "":
		t := "desc"
		filters.Sort = &t
	case "asc", "desc":
		filters.Sort = &listSort
	default:
		return fmt.Errorf("%s is an invalid sorting value", listSort)
	}

	if listMilestone != "" {
		filters.Milestone = &listMilestone
	}

	if listLabel != "" {
		filters.Labels = gitlab.Labels{listLabel}
	}

	if listNotLabel != "" {
		filters.NotLabels = gitlab.Labels{listNotLabel}
	}

	switch listScope {
	case "all", "":
		filters.Scope = &listScope
	case "assigned-to-me":
		t := "assigned_to_me"
		filters.Scope = &t
	case "created-by-me":
		t := "created_by_me"
		filters.Scope = &t
	default:
		return fmt.Errorf("%s is an invalid scope to filter", listScope)
	}

	if listSource != "" {
		filters.SourceBranch = &listSource
	}

	if listTarget != "" {
		filters.TargetBranch = &listTarget
	}

	if listSearch != "" {
		filters.Search = &listSearch
	}

	switch listWIP {
	case "":
	case "yes", "no":
		filters.WIP = &listWIP
	}

	mrs, _, err := GitlabClient.MergeRequests.ListProjectMergeRequests(
		repoSpec,
		&filters,
		nil,
	)
	if err != nil {
		return err
	}

	if len(mrs) < 1 {
		return errors.New("no Merge Requests were found with your filter options")
	}

	width, _, err := terminal.GetSize(int(os.Stdin.Fd()))
	if err != nil {
		width = 80
	}

	// 80 is a sane default, 80 is what people commonly use but I think we can shoot for me
	if width < 80 {
		width = 80
	}

	for _, mr := range mrs {
		expectedStateSize := len(fmt.Sprintf("%s | ", mr.State))               // Expected Size of State block
		expectedIIDSize := len(fmt.Sprintf("!%d | ", mr.IID))                  // Expected Size of IID block
		expectedUsernameSize := len(fmt.Sprintf("@%s | ", mr.Author.Username)) // Expected size of By: block
		expectedTitleSize := len(fmt.Sprintf("%s", mr))                        // Expected Size of Title block
		expectedFinalSize := expectedStateSize + expectedIIDSize + expectedUsernameSize + expectedTitleSize

		// Title that will be printed, subject to truncation
		finalTitle := mr.Title

		// Check if we have or not columns to spare
		if expectedFinalSize > width {
			// How many characters will be left out, add 3 to it for the '...', we should
			// have how many characters we need to truncate to fit into the screen after
			// adding the '...'
			// the 1 comes from the whitespace after the last '|'
			maxLen := width - (expectedStateSize + expectedIIDSize + expectedUsernameSize + 1)
			finalTitle = helpers.TruncateString(finalTitle, maxLen)
		}

		// Give green coloring for open and merged merge requests
		// and red coloring for closed and locked merge requests
		var issueState string

		switch mr.State {
		case "opened":
			issueState = cPositive.Sprint("open")
		case "merged":
			issueState = cPositive.Sprint(mr.State)
		case "closed", "locked":
			issueState = cNegative.Sprint(mr.State)
		}

		fmt.Printf("%s | !%s | @%s | %s\n",
			issueState,
			cBold.Render(mr.IID),
			cUser.Sprint(mr.Author.Username),
			cObject.Sprint(finalTitle),
		)
	}
	return nil
}
