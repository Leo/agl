package mr

import (
	"bytes"
	"errors"
	"fmt"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strings"

	git "github.com/libgit2/git2go/v31"
	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

var (
	// checkoutRemote is the name of the remote that contains the URL for the repo
	// from the URL we derive the NAMESPACE/PATH and use it for making all necessary
	// calls
	checkoutRemote string
	// checkoutBranch is the name of the branch we will check out to, it defaults
	// to the number of the MR, or the name of the branch if used, can be overridden
	// with {-b|--branch}
	checkoutBranch string
	// sourceBranch is the name of the branch we will check out before creating our
	// branch, this defaults to master and can be changed with {-s|--source}
	sourceBranch string

	// Check if a string is a digit
	digitCheck = regexp.MustCompile(`^[0-9]+$`)

	// Checkout a merge request locally
	Checkout = &cobra.Command{
		Use:   "checkout",
		Short: "checkout a merge request locally in separate branch",
		Long: `checkout a merge request locally in separate branch
		
Supply the merge request as:		
- Internal ID, e.g. 123
- URL to the issue, e.g. https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/123
- Name of the source branch, e.g. source-branch-for-mr-123

When using internal ID or source branch name, the project URL is picked from the remote "upstream"
in the repository, use --remote to change name of the remote to check.

When giving the name of the source branch one can prefix OWNER: to indicate the username of who
created the source branch, otherwise it will match all instances of the source branch and will
ask the user to pick which one they want.
`,
		Example: `
$ agl mr checkout 1
$ agl mr checkout https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/123
$ agl mr checkout patch-1`,
		Args: cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			if err := checkoutMergeRequest(args[0]); err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
		},
	}
)

func init() {
	Checkout.Flags().StringVarP(
		&checkoutRemote,
		"remote",
		"r",
		"upstream",
		"remote to check for project URL",
	)
	Checkout.Flags().StringVarP(
		&checkoutBranch,
		"branch",
		"b",
		"",
		"name of checkout branch",
	)
	Checkout.Flags().StringVarP(
		&sourceBranch,
		"source",
		"s",
		"master",
		"if creating a new branch, check out commit on top of this one",
	)
}

func checkoutMergeRequest(arg string) error {

	// It is absolutely necessary we be in a git repository
	r, err := git.OpenRepository(".") // PWD
	if err != nil {
		return err
	}

	// Check if we were not given an URL, we need all to be URLs in the end
	if !strings.Contains(arg, "http://") {
		// Open the repository that we are currently in, we use it to find remotes

		remote, err := r.Remotes.Lookup(checkoutRemote)
		if err != nil {
			return err
		}

		endpoint := remote.Url()

		// Convert whatever transport we got into HTTPS, we only support http due
		// to usage of the Personal Access Token
		endpoint, err = helpers.ConvertToHTTPS(endpoint)
		if err != nil {
			return err
		}

		if digitCheck.MatchString(arg) {
			// endpoint is the URL of the repo like https://gitlab.alpinelinux.org/alpine/aports
			// arg in this case is a digit, we add it at the end to get the full URL to the repo
			arg = fmt.Sprintf("%s/-/merge_requests/%s", endpoint, arg)
		} else {
			// It is not a digit, so assume it is a branch name and try to find a branch that
			// matches it
			GitlabClient, err := helpers.GetClient()
			if err != nil {
				return err
			}

			repoSpec, err := helpers.GetNamespacePathFromURL(remote.Url())
			if err != nil {
				return err
			}

			var owner string

			// Check if we have an OWNER: given
			if strings.Contains(arg, ":") {
				owner = strings.Split(arg, ":")[0] // OWNER: component
				arg = strings.Split(arg, ":")[1]   // :BRANCH component
			}

			// --branch wasn't passed to us, use the branch
			if checkoutBranch == "" {
				checkoutBranch = arg
			}

			mrs, _, err := GitlabClient.MergeRequests.ListProjectMergeRequests(
				repoSpec,
				&gitlab.ListProjectMergeRequestsOptions{
					SourceBranch: &arg,
				},
				nil,
			)
			if err != nil {
				return fmt.Errorf("couldn't fetch merge requests with source branch %s", arg)
			}

			if len(mrs) == 0 {
				return errors.New("no merge requests match your criteria")
			} else if len(mrs) == 1 {
				if owner != "" && owner != mrs[0].Author.Username {
					return fmt.Errorf("no merge requests from %s with source branch %s", owner, arg)
				}
				arg = mrs[0].WebURL
			} else if len(mrs) > 1 && owner != "" {
				mrMap := make(map[string]string)
				for _, mr := range mrs {
					if owner == mr.Author.Username {
						mrMap[mr.Author.Username] = mr.WebURL
					}
				}
				var ok bool
				// Try to access the WebURL via the username
				arg, ok = mrMap[owner]
				if !ok {
					return fmt.Errorf("no source branches named %s from %s in the merge requests for %s",
						arg,
						owner,
						repoSpec,
					)
				}
			} else if len(mrs) > 1 && owner == "" {
				var mrPick []string
				mrMap := make(map[string]*gitlab.MergeRequest)
				for _, mr := range mrs {
					t := fmt.Sprintf("%s by %s", mr.WebURL, mr.Author.Username)
					mrPick = append(mrPick, t)
					mrMap[t] = mr
				}

				// Ask the user to pick a merge request
				askMR := promptui.Select{
					Label: "pick a merge request",
					Items: mrPick,
				}
				_, pickedMR, err := askMR.Run()
				if err != nil {
					return err
				}
				arg = mrMap[pickedMR].WebURL
			}
		}
	}

	// custom error, returned whenever there is a redirect
	var RedirectAttemptedError = errors.New("redirect")

	// http client that returns error when redirected
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return RedirectAttemptedError
		},
	}

	resp, err := client.Get(arg + ".patch")
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Lookup a branch
	pickedBranch, err := r.LookupBranch(sourceBranch, git.BranchAll)
	if err != nil {
		return err
	}

	// Get the head commit
	headRef := pickedBranch.Target()

	// Lookup the commit referenced by the head commit
	headCommit, err := r.LookupCommit(headRef)
	if err != nil {
		return err
	}

	if checkoutBranch == "" {
		// Use MR number
		argLen := len(strings.Split(arg, "/"))
		checkoutBranch = strings.Split(arg, "/")[argLen-1]
	}

	// Check if we are already in the branch
	if checkoutBranch != pickedBranch.Shorthand() {

		// Create branch
		localBranch, err := r.CreateBranch(
			checkoutBranch,
			headCommit,
			true,
		)
		if err != nil {
			return err
		}

		// Make HEAD reference our branch
		// The true is absolutely necessary
		_, err = r.References.CreateSymbolic("HEAD", "refs/heads/"+localBranch.Shorthand(), true, "")
		if err != nil {
			return err
		}

		// Checkout the new branch we created
		err = r.CheckoutHead(&git.CheckoutOpts{
			Strategy: git.CheckoutForce,
		})
		if err != nil {
			return err
		}

		var remote string
		if viper.GetString("picked_remote") != "" {
			remote = viper.GetString("picked_remote")
		} else if repoSpec := helpers.GetRemotePathSpec("upstream"); repoSpec != "" {
			remote = "upstream"
		} else if repoSpec := helpers.GetRemotePathSpec("origin"); repoSpec != "" {
			remote = "origin"
		} else {
			return nil
		}

		err = localBranch.SetUpstream(remote + "/" + sourceBranch)
		if err != nil {
			return err
		}
	}

	// run 'git am -3' on the branch we just checked out to, make stdin the input we got
	gitExecutable, err := exec.LookPath("git")
	if err != nil {
		return err
	}
	cmd := exec.Command(gitExecutable, "am", "-3")
	cmd.Stdin = resp.Body

	var stderr bytes.Buffer
	cmd.Stdout = os.Stdout
	cmd.Stderr = &stderr
	err = cmd.Run()
	if err != nil {
		// Try aborting the merge, this happens when there is
		// a conflict
		cmd = exec.Command(gitExecutable, "merge", "--abort")
		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		_ = cmd.Run()

		if stderr.Len() == 0 {
			return err
		}
		return fmt.Errorf("%s: %s", err, stderr.String())
	}

	return nil
}
