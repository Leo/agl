package mr

import (
	"testing"
)

func TestIsWIP(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		desc string
		in   string
	}{
		{"Standard Keyword WIP:", "WIP:"},
		{"Standard Keyword WIP: (mixed-case)", "wIP:"},
		{"Standard Keyword WIP: (mixed-case)", "WiP:"},
		{"Standard Keyword WIP: (mixed-case)", "wiP:"},
		{"Standard Keyword WIP: (mixed-case)", "WIp:"},
		{"Standard Keyword WIP: (mixed-case)", "wIp:"},
		{"Standard Keyword WIP: (mixed-case)", "Wip:"},
		{"Standard Keyword WIP: (lower-case)", "wip:"},

		{"Standard Keyword [WIP]", "[WIP]"},
		{"Standard Keyword [WIP] (mixed-case)", "[wIP]"},
		{"Standard Keyword [WIP] (mixed-case)", "[WiP]"},
		{"Standard Keyword [WIP] (mixed-case)", "[wiP]"},
		{"Standard Keyword [WIP] (mixed-case)", "[WIp]"},
		{"Standard Keyword [WIP] (mixed-case)", "[wIp]"},
		{"Standard Keyword [WIP] (mixed-case)", "[Wip]"},
		{"Standard Keyword [WIP] (lower-case)", "[wip]"},

		{"Standard Keyword [Draft]", "[Draft]"},
		{"Standard Keyword [Draft] (upper-case)", "[DRAFT]"},
		{"Standard Keyword [Draft] (mixed-case)", "[dRAFT]"},
		{"Standard Keyword [Draft] (mixed-case)", "[DrAFT]"},
		{"Standard Keyword [Draft] (mixed-case)", "[drAFT]"},
		{"Standard Keyword [Draft] (mixed-case)", "[DRaFT]"},
		{"Standard Keyword [Draft] (mixed-case)", "[dRaFT]"},
		{"Standard Keyword [Draft] (mixed-case)", "[DraFT]"},
		{"Standard Keyword [Draft] (mixed-case)", "[draFT]"},
		{"Standard Keyword [Draft] (mixed-case)", "[DRAfT]"},
		{"Standard Keyword [Draft] (mixed-case)", "[dRAfT]"},
		{"Standard Keyword [Draft] (mixed-case)", "[DrAfT]"},
		{"Standard Keyword [Draft] (mixed-case)", "[drAfT]"},
		{"Standard Keyword [Draft] (mixed-case)", "[DRafT]"},
		{"Standard Keyword [Draft] (mixed-case)", "[dRafT]"},
		{"Standard Keyword [Draft] (mixed-case)", "[DrafT]"},
		{"Standard Keyword [Draft] (mixed-case)", "[drafT]"},
		{"Standard Keyword [Draft] (mixed-case)", "[DRAFt]"},
		{"Standard Keyword [Draft] (mixed-case)", "[dRAFt]"},
		{"Standard Keyword [Draft] (mixed-case)", "[DrAFt]"},
		{"Standard Keyword [Draft] (mixed-case)", "[drAFt]"},
		{"Standard Keyword [Draft] (mixed-case)", "[DRaFt]"},
		{"Standard Keyword [Draft] (mixed-case)", "[dRaFt]"},
		{"Standard Keyword [Draft] (mixed-case)", "[DraFt]"},
		{"Standard Keyword [Draft] (mixed-case)", "[draFt]"},
		{"Standard Keyword [Draft] (mixed-case)", "[DRAft]"},
		{"Standard Keyword [Draft] (mixed-case)", "[dRAft]"},
		{"Standard Keyword [Draft] (mixed-case)", "[DrAft]"},
		{"Standard Keyword [Draft] (mixed-case)", "[drAft]"},
		{"Standard Keyword [Draft] (mixed-case)", "[DRaft]"},
		{"Standard Keyword [Draft] (mixed-case)", "[dRaft]"},
		{"Standard Keyword [Draft] (lower-case)", "[draft]"},

		{"Standard Keyword Draft:", "Draft:"},
		{"Standard Keyword Draft: (upper-case)", "DRAFT:"},
		{"Standard Keyword Draft: (mixed-case)", "dRAFT:"},
		{"Standard Keyword Draft: (mixed-case)", "DrAFT:"},
		{"Standard Keyword Draft: (mixed-case)", "drAFT:"},
		{"Standard Keyword Draft: (mixed-case)", "DRaFT:"},
		{"Standard Keyword Draft: (mixed-case)", "dRaFT:"},
		{"Standard Keyword Draft: (mixed-case)", "DraFT:"},
		{"Standard Keyword Draft: (mixed-case)", "draFT:"},
		{"Standard Keyword Draft: (mixed-case)", "DRAfT:"},
		{"Standard Keyword Draft: (mixed-case)", "dRAfT:"},
		{"Standard Keyword Draft: (mixed-case)", "DrAfT:"},
		{"Standard Keyword Draft: (mixed-case)", "drAfT:"},
		{"Standard Keyword Draft: (mixed-case)", "DRafT:"},
		{"Standard Keyword Draft: (mixed-case)", "dRafT:"},
		{"Standard Keyword Draft: (mixed-case)", "DrafT:"},
		{"Standard Keyword Draft: (mixed-case)", "drafT:"},
		{"Standard Keyword Draft: (mixed-case)", "DRAFt:"},
		{"Standard Keyword Draft: (mixed-case)", "dRAFt:"},
		{"Standard Keyword Draft: (mixed-case)", "DrAFt:"},
		{"Standard Keyword Draft: (mixed-case)", "drAFt:"},
		{"Standard Keyword Draft: (mixed-case)", "DRaFt:"},
		{"Standard Keyword Draft: (mixed-case)", "dRaFt:"},
		{"Standard Keyword Draft: (mixed-case)", "DraFt:"},
		{"Standard Keyword Draft: (mixed-case)", "draFt:"},
		{"Standard Keyword Draft: (mixed-case)", "DRAft:"},
		{"Standard Keyword Draft: (mixed-case)", "dRAft:"},
		{"Standard Keyword Draft: (mixed-case)", "DrAft:"},
		{"Standard Keyword Draft: (mixed-case)", "drAft:"},
		{"Standard Keyword Draft: (mixed-case)", "DRaft:"},
		{"Standard Keyword Draft: (mixed-case)", "dRaft:"},
		{"Standard Keyword Draft: (lower-case)", "draft:"},

		{"Standard Keyword (Draft)", "(Draft)"},
		{"Standard Keyword (Draft) (upper-case)", "(DRAFT)"},
		{"Standard Keyword (Draft) (mixed-case)", "(dRAFT)"},
		{"Standard Keyword (Draft) (mixed-case)", "(DrAFT)"},
		{"Standard Keyword (Draft) (mixed-case)", "(drAFT)"},
		{"Standard Keyword (Draft) (mixed-case)", "(DRaFT)"},
		{"Standard Keyword (Draft) (mixed-case)", "(dRaFT)"},
		{"Standard Keyword (Draft) (mixed-case)", "(DraFT)"},
		{"Standard Keyword (Draft) (mixed-case)", "(draFT)"},
		{"Standard Keyword (Draft) (mixed-case)", "(DRAfT)"},
		{"Standard Keyword (Draft) (mixed-case)", "(dRAfT)"},
		{"Standard Keyword (Draft) (mixed-case)", "(DrAfT)"},
		{"Standard Keyword (Draft) (mixed-case)", "(drAfT)"},
		{"Standard Keyword (Draft) (mixed-case)", "(DRafT)"},
		{"Standard Keyword (Draft) (mixed-case)", "(dRafT)"},
		{"Standard Keyword (Draft) (mixed-case)", "(DrafT)"},
		{"Standard Keyword (Draft) (mixed-case)", "(drafT)"},
		{"Standard Keyword (Draft) (mixed-case)", "(DRAFt)"},
		{"Standard Keyword (Draft) (mixed-case)", "(dRAFt)"},
		{"Standard Keyword (Draft) (mixed-case)", "(DrAFt)"},
		{"Standard Keyword (Draft) (mixed-case)", "(drAFt)"},
		{"Standard Keyword (Draft) (mixed-case)", "(DRaFt)"},
		{"Standard Keyword (Draft) (mixed-case)", "(dRaFt)"},
		{"Standard Keyword (Draft) (mixed-case)", "(DraFt)"},
		{"Standard Keyword (Draft) (mixed-case)", "(draFt)"},
		{"Standard Keyword (Draft) (mixed-case)", "(DRAft)"},
		{"Standard Keyword (Draft) (mixed-case)", "(dRAft)"},
		{"Standard Keyword (Draft) (mixed-case)", "(DrAft)"},
		{"Standard Keyword (Draft) (mixed-case)", "(drAft)"},
		{"Standard Keyword (Draft) (mixed-case)", "(DRaft)"},
		{"Standard Keyword (Draft) (mixed-case)", "(dRaft)"},
		{"Standard Keyword (Draft) (lower-case)", "(draft)"},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			t.Parallel()
			out := isWIP(tC.in)
			if out == nil {
				t.Errorf("expected marker %s got nil", tC.in)
			}
			if *out != tC.in {
				t.Errorf("expected marker %s got %s", tC.in, *out)
			}
		})
	}
}
