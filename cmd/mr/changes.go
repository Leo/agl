package mr

import (
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

var (
	// Changes shows what changes will be done by a merge request
	Changes = &cobra.Command{
		Use:     "changes <mrnum || URL>",
		Aliases: []string{"diff"},
		Short:   "show changes in a merge request",
		Args:    cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			// Check if merge request is actually num
			mrNum, err := strconv.Atoi(args[0])
			if err != nil {
				var repoSpec string
				repoSpec, mrNum, err = helpers.FindMergeRequestViaURL(args[0])
				if err != nil {
					fmt.Printf("%s %s\n", promptui.IconBad, err)
					os.Exit(1)
				}
				viper.Set("picked_repo", repoSpec)
			}
			if err := showMergeRequestChanges(mrNum); err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
		},
	}
)

func init() {
	Changes.Flags().BoolVarP(
		&dryRun,
		"dry-run",
		"n",
		false,
		"show from which merge request it would show the pipelines",
	)
}

func showMergeRequestChanges(mrNum int) error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}

	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	if dryRun {
		fmt.Printf("Would show changes from merge request !%s in %s\n",
			cBold.Sprint(mrNum),
			cBold.Sprint(repoSpec),
		)
		return nil
	}
	diffs, _, err := GitlabClient.MergeRequests.GetMergeRequestChanges(
		repoSpec,
		mrNum,
		nil,
	)
	if err != nil {
		return err
	}

	var totalDiff []string

	for _, diff := range diffs.Changes {
		totalDiff = append(totalDiff, fmt.Sprintf("--- a/%s\n+++ b/%s\n%s",
			diff.OldPath,
			diff.NewPath,
			diff.Diff,
		))
	}

	pickedPager := os.ExpandEnv("$PAGER")
	if pickedPager == "" {
		pickedPager = "less"
	}

	pagerCmd, err := exec.LookPath(pickedPager)
	if err != nil {
		return err
	}

	cmd := exec.Command(pagerCmd)

	// Create a reader with all the diffs we got, each diff is one element of the slice
	// so join them with a '\n' to avoid one writing over the other
	cmd.Stdin = strings.NewReader(strings.Join(totalDiff, "\n"))

	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err = cmd.Run()
	if err != nil {
		return err
	}

	return nil
}
