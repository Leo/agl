package mr

import (
	"fmt"
	"os"
	"strconv"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

// Close an merge request that was previously closed
var Close = &cobra.Command{
	Use:   "close <mrnum || URL>",
	Short: "close a merge request either",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		// Check if merge request is actually num
		mrNum, err := strconv.Atoi(args[0])
		if err != nil {
			var repoSpec string
			repoSpec, mrNum, err = helpers.FindMergeRequestViaURL(args[0])
			if err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
			viper.Set("picked_repo", repoSpec)
		}
		if err := closeMergeRequest(mrNum); err != nil {
			fmt.Printf("%s %s\n", promptui.IconBad, err)
			os.Exit(1)
		}
	},
}

func init() {
	Close.Flags().BoolVarP(&dryRun, "dry-run", "n", false, "don't close the merge request, show which it would close")
}

func closeMergeRequest(mrNum int) error {
	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}
	state := "close"

	if dryRun {
		fmt.Printf("Would close merge request !%s in %s\n",
			cBold.Sprint(mrNum),
			cBold.Sprint(repoSpec),
		)
		return nil
	}
	fmt.Printf("%s closeing merge request !%s in %s\n",
		promptui.IconInitial,
		cBold.Sprint(mrNum),
		cBold.Sprint(repoSpec),
	)
	err = updateMR(repoSpec, mrNum, gitlab.UpdateMergeRequestOptions{StateEvent: &state})
	if err != nil {
		return err
	}
	fmt.Printf("%s closed merge request !%s in %s\n",
		promptui.IconGood,
		cBold.Sprint(mrNum),
		cBold.Sprint(repoSpec),
	)
	return nil
}
