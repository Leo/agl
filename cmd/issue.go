package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.alpinelinux.org/Leo/agl/cmd/issue"
)

var issueCmd = &cobra.Command{
	Use:   "issue",
	Short: "manage issues",
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

func init() {
	rootCmd.AddCommand(issueCmd)

	issueCmd.AddCommand(issue.Close)
	issueCmd.AddCommand(issue.Reopen)
	issueCmd.AddCommand(issue.Create)
	issueCmd.AddCommand(issue.Edit)
	issueCmd.AddCommand(issue.View)
	issueCmd.AddCommand(issue.List)
}
