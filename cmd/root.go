package cmd

import (
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/constants"
)

type profile struct {
	host     string // URL of the GitLab instance
	token    string // Personal Access Token
	userID   int    `mapstructure:"user_id"` // ID of the user
	username string
}

// Config holds all the profiles
type Config struct {
	defaultProfile string            `mapstructure:"default_profile"`
	binds          map[string]string // Map a repository path to a profile
	profiles       []profile
}

var (
	cfgFile string // Not meant for configuration
	// Profile holds the value of --profile, used to pick a different profile to use
	prof      string // Not meant for configuration
	create    bool   // When --profile is passed but it doesn't exist, create it
	bindTo    string // Bind current working directory to a profile
	overwrite bool   // When bind is used on a path that already exists, overwrite the previous value
	repoPath  string // used with --repo to indicate NAMESPACE/PATH of a repo, stored as picked_repo with Viper
	remote    string // user with --remote to indicate a remote to look at, stored as picked_remote with Viper

	rootCmd = &cobra.Command{
		Use:   "agl",
		Short: "Work from the commandline on Alpine Linux's GitLab instance",
		Long: `CLI tool for dealing with Alpine Linux's GitLab instance, includes everything
from forking, cloning, creating and deleting repos to creating merge requests, viewing
diffs, creating issues and releases.`,
		Args: cobra.NoArgs,
		PreRun: func(cmd *cobra.Command, args []string) {
			// If we were passed --create then make sure that --profile
			// is also set
			if create && prof == "" {
				log.Fatalf("%s --create requires --profile to be set", promptui.IconBad)
			}
		},
		Run: func(cmd *cobra.Command, args []string) {
			// If the user passed --bind-to then run just that and exit
			if bindTo != "" {
				err := bindPath(bindTo, overwrite)
				if err != nil {
					fmt.Printf("%s %s\n", promptui.IconBad, err)
				}
			} else {
				cmd.Help()
			}
		},
	}
)

// Execute executes the root command
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.config/agl/config.toml)")
	rootCmd.PersistentFlags().StringVar(&prof, "profile", "", "Use given profile instead of default")
	rootCmd.PersistentFlags().BoolVar(&create, "create", false, "create given profile if it doesn't exist")
	rootCmd.PersistentFlags().StringVar(&repoPath,
		"repo",
		"",
		"select another repository with [NAMESPACE/]PATH format or URL, overrides --remote",
	)
	rootCmd.PersistentFlags().StringVar(&remote,
		"remote",
		"",
		"when using URLs of remotes to find target project, use this remote first",
	)
	rootCmd.Flags().StringVar(&bindTo, "bind-to", "", "bind current working directory to profile")
	rootCmd.Flags().BoolVar(&overwrite, "overwrite", false, "overwrite path if already bound")
}

func initConfig() {
	viper.SetConfigType("yaml")
	viper.SetConfigName("config")

	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	}

	confdir, err := os.UserConfigDir()
	if err != nil {
		log.Fatal(err)
	}
	confdir = filepath.Join(confdir, "agl")
	viper.AddConfigPath(confdir)

	err = viper.ReadInConfig()
	if err != nil {
		// File not found, create directory and write empty configuration file
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			_, err = os.Stat(confdir)
			if !os.IsExist(err) {
				os.MkdirAll(confdir, 0771)
			}
			err = viper.SafeWriteConfig()
			if err != nil {
				log.Fatal(err)
			}
		} else {
			log.Fatal(err)
		}
	}

	// We were passed --bind-to, exit early
	if bindTo != "" {
		return
	}

	var profile string

	// Use --profile as given profile
	if prof != "" {
		// Not documented, Get returns nil when nothing is found
		if viper.Get("profiles."+prof) != nil {
			// There is a defined profile
			profile = prof
		} else {
			// If the user didn't pass --create then error away
			if !create {
				log.Fatalf("%s profile %s does not exist, run with --create to create it", promptui.IconBad, prof)
			}

			// Set the profile name to the one given to us, later in the code we will ask the
			// rest of the variables
			profile = prof
		}
	} else {
		// We were not passed --profile, try to get from our binds, which bind a path
		// to a profile
		profileMap := viper.GetStringMapString("binds")
		wd, err := os.Getwd()
		if err != nil {
			log.Fatal(err)
		}

		// Viper only has case-insensitive and returns everything in lowercase
		// so we have to be in lowercase too
		wd = strings.ToLower(wd)

		var ok bool
		// Check in the profile map for our directory and its associated profile
		profile, ok = profileMap[wd]

		// It doesn't exist, get the default profile
		if !ok {
			// Get the default profile
			profile = viper.GetString("default_profile")
		} else {
			// We have a bind, check if the profile exists because we don't
			// want to start creating a new profile because the user created
			// a bind to a non-existent profile
			if viper.Sub("profiles."+profile) == nil {
				log.Fatalf("%s profile %s bound to path %s does not exist",
					promptui.IconBad,
					profile,
					wd,
				)
			}
		}

		// If default is empty ask user to set a default profile
		if profile == "" {
			fmt.Printf("%s no profiles are defined, making a new one\n", promptui.IconInitial)
			profile, err = askProfileName()
			if err != nil {
				log.Fatal(err)
			}
			viper.Set("default_profile", profile)
			viper.WriteConfig()
		}
	}

	// Ask user for host if it doesn't exist
	if viper.GetString("profiles."+profile+".host") == "" {
		host, err := askHost()
		if err != nil {
			log.Fatal(err)
		}
		viper.Set("profiles."+profile+".host", host)
		viper.WriteConfig()
	}

	// Ask user for Personal Access Token (PAT)
	if viper.GetString("profiles."+profile+".token") == "" {
		token, err := askPAT()
		if err != nil {
			log.Fatal(err)
		}
		viper.Set("profiles."+profile+".token", token)
		viper.WriteConfig()
	}

	// Get the user
	if viper.GetString("profiles."+profile+".user_id") == "" ||
		viper.GetString("profiles."+profile+".username") == "" {

		gl, err := gitlab.NewClient(
			viper.GetString("profiles."+profile+".token"),
			gitlab.WithBaseURL(fmt.Sprintf("https://%s", viper.GetString("profiles."+profile+".host"))),
		)
		if err != nil {
			log.Fatal(err)
		}
		res, _, err := gl.Users.CurrentUser()
		if err != nil {
			log.Fatal(err)
		}
		viper.Set("profiles."+profile+".user_id", res.ID)
		viper.Set("profiles."+profile+".username", res.Username)
		viper.WriteConfig()
	}

	// Set profile so that other commands can use it
	// do this only at the end after all WriteConfig() have been called
	viper.Set("profile", profile)

	// --repo was passed, set it so our consumers can use it
	if repoPath != "" {
		viper.Set("picked_repo", repoPath)
	}

	// --remote was passed, set it so our consumers can use it
	if remote != "" {
		viper.Set("picked_remote", remote)
	}
}

func askProfileName() (string, error) {
	prompt := promptui.Prompt{
		Label: "name of profile",
		Validate: func(s string) error {
			if s == "" {
				return errors.New("")
			}
			return nil
		},
	}
	name, err := prompt.Run()
	if err != nil {
		return "", err
	}
	return name, nil
}

func askHost() (string, error) {
	prompt := promptui.Select{
		Label: "pick a supported host",
		Items: constants.Hosts,
	}
	_, host, err := prompt.Run()
	if err != nil {
		return "", err
	}
	return host, nil
}

func askPAT() (string, error) {
	prompt := promptui.Prompt{
		Label: "Personal Access Token",
		Mask:  '*',
		Validate: func(s string) error {
			if s == "" {
				return errors.New("")
			}
			return nil
		},
	}
	token, err := prompt.Run()
	if err != nil {
		return "", err
	}
	return token, nil
}

func bindPath(bindTo string, overwrite bool) error {
	// Current working directory
	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	// profile does not exist
	if viper.Sub("profiles."+bindTo) == nil {
		return fmt.Errorf("profile %s does not exist", bindTo)
	}

	// Check if path is already bound
	bindMap := viper.GetStringMapString("binds")
	val, ok := bindMap[wd]

	// If key exists
	if ok {
		// If the profile of the key isn't the same as the one we want to bind and we didn't pass --overwrite
		if val != bindTo && !overwrite {
			return fmt.Errorf("%s is bound to %s, --overwrite to force rebound", wd, bindTo)
		}

		// If the profile of the key is the same as the one we want to bind
		if val == bindTo {
			fmt.Printf("%s %s is already bound to %s\n", promptui.IconWarn, wd, bindTo)
			return nil
		}
	}

	// bind Path to Profile
	viper.Set("binds."+wd, bindTo)
	viper.WriteConfig()

	fmt.Printf("%s bound path %s to profile %s\n", promptui.IconGood, wd, bindTo)

	return nil
}
