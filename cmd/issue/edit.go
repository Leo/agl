package issue

import (
	"errors"
	"fmt"
	"os"
	"strconv"

	"github.com/google/go-cmp/cmp"
	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/constants"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

var (
	issueEditTitle            string
	issueEditDescription      bool
	issueEditConfidential     string
	issueEditLabels           string
	issueEditAddLabels        string
	issueEditRemoveLabels     string
	issueEditStateEvent       string
	issueEditDiscussionLocked string

	// Edit an existing issue
	Edit = &cobra.Command{
		Use:   "edit <issuenum || URL>",
		Short: "edit attributes of an issue",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			// Check if issue is actually num
			issueNum, err := strconv.Atoi(args[0])
			if err != nil {
				var repoSpec string
				repoSpec, issueNum, err = helpers.FindIssueViaURL(args[0])
				if err != nil {
					fmt.Printf("%s %s\n", promptui.IconBad, err)
					os.Exit(1)
				}
				viper.Set("picked_repo", repoSpec)
			} else {
				if err := editIssue(issueNum); err != nil {
					fmt.Printf("%s %s\n", promptui.IconBad, err)
					os.Exit(1)
				}
			}
		},
	}
)

func init() {
	Edit.Flags().StringVar(&issueEditTitle,
		"title",
		"",
		"set title",
	)
	Edit.Flags().BoolVar(&issueEditDescription,
		"description",
		false,
		"set description via $EDITOR",
	)
	Edit.Flags().StringVar(&issueEditConfidential,
		"confidential",
		"",
		"set confidentiality: {yes|no}",
	)
	Edit.Flags().StringVar(&issueEditLabels,
		"labels",
		"",
		"comma-separated list of labels to set",
	)
	Edit.Flags().StringVar(&issueEditAddLabels,
		"add-labels",
		"",
		"comma-separated list of labels to add",
	)
	Edit.Flags().StringVar(&issueEditRemoveLabels,
		"remove-labels",
		"",
		"comma-separated list of labels to remove",
	)
	Edit.Flags().StringVar(&issueEditStateEvent,
		"state",
		"",
		"set state: {open|closed}",
	)
	Edit.Flags().StringVar(&issueEditDiscussionLocked,
		"discussion",
		"",
		"set discussion permission: {locked|unlocked}",
	)
}

func editIssue(issueNum int) error {
	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	opts := gitlab.UpdateIssueOptions{}

	if issueEditTitle != "" {
		opts.Title = &issueEditTitle
	}

	if issueEditDescription {
		desc, err := constants.EditWithEditor(nil)
		if err != nil {
			fmt.Printf("%s failed to get description due to %s\n", promptui.IconBad, err)
		} else if desc == nil && *desc == "" {
			fmt.Printf("%s description must not be empty\n", promptui.IconBad)
		} else {
			opts.Description = desc
		}
	}

	switch issueEditConfidential {
	case "":
		// No value was given to us, continue as normal
		break
	case "yes":
		t := true
		opts.Confidential = &t
	case "no":
		t := false
		opts.Confidential = &t
	default:
		fmt.Printf("%s --confidential must be yes|no, got %s\n", promptui.IconBad, issueEditConfidential)
	}

	if issueEditLabels != "" && issueEditAddLabels != "" {
		return errors.New("--labels and --add-labels are mutually exclusive, add the values of --add-labels to --labels")
	}
	if issueEditLabels != "" && issueEditRemoveLabels != "" {
		return errors.New("--remove-labels with --labels is redundant, --labels will remove all labels not in it")
	}

	if issueEditLabels != "" {
		opts.Labels = gitlab.Labels{issueEditLabels}
	}

	if issueEditAddLabels != "" {
		opts.AddLabels = gitlab.Labels{issueEditAddLabels}
	}

	if issueEditRemoveLabels != "" {
		opts.RemoveLabels = gitlab.Labels{issueEditRemoveLabels}
	}

	switch issueEditStateEvent {
	case "":
		// No value was given to us, continue as normal
		break
	case "open":
		t := "reopen"
		opts.StateEvent = &t
	case "closed":
		t := "close"
		opts.StateEvent = &t
	default:
		fmt.Printf("%s --state must be open|closed, got %s\n", promptui.IconBad, issueEditStateEvent)
	}

	switch issueEditDiscussionLocked {
	case "":
		// No value was given to us, continue as normal
		break
	case "locked":
		t := true
		opts.DiscussionLocked = &t
	case "unlocked":
		t := false
		opts.DiscussionLocked = &t
	default:
		fmt.Printf("%s --discussion must be locked|unlocked, got %s\n", promptui.IconBad, issueEditDiscussionLocked)
	}

	// Check if we were given any parameters
	if cmp.Equal(opts, gitlab.UpdateIssueOptions{}) {
		return errors.New("no new values were given to us")
	}

	// Update issue with our changes
	fmt.Printf("%s updating #%s from %s\n", promptui.IconInitial, cBold.Sprint(issueNum), repoSpec)
	err = updateIssue(repoSpec, issueNum, opts)
	if err != nil {
		return err
	}
	fmt.Printf("%s updated #%s from %s\n", promptui.IconGood, cBold.Sprint(issueNum), repoSpec)

	return nil
}

func updateIssue(repoSpec string, issueNum int, opts gitlab.UpdateIssueOptions) error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}

	_, _, err = GitlabClient.Issues.UpdateIssue(
		repoSpec,
		issueNum,
		&opts,
		nil,
	)
	if err != nil {
		return err
	}
	return nil
}
