package issue

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/constants"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

var (
	// Create an issue
	Create = &cobra.Command{
		Use:   "create",
		Short: "create issue in a project",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			if err := createIssue(); err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
		},
	}
)

func init() {
	Create.Flags().BoolVarP(&dryRun, "dry-run", "n", false, "don't create the issue, show what it would look like")
}

func createIssue() error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}

	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	opts := &gitlab.CreateIssueOptions{}

	/*
	 * TODO: Handle every single option through the usage of a select prompt that
	 * allows users to pick an attribute and an specific prompt for each type of
	 * attribute (example selection for a boolean or multiple-choice, and a prompt
	 * for ones that require an attribute)
	 *
	 * Implemented (Either here or elsewhere in Code due to be obligatory):
	 *  - Title
	 *  - Description
	 *  - Labels
	 *  - Confidential
	 * Rejected Implementations:
	 *  - AssigneeID(s) (Requires Int no way to use Username)
	 *  - MilestoneID (Requires Int, no way to use Name)
	 *
	 * Reference: https://godoc.org/github.com/xanzy/go-gitlab#CreateIssueOptions
	 * Reference: https://docs.gitlab.com/ee/api/issues.html#new-issue
	 */
Continue:
	// Infinite loop until continue is picked
	for {
		// Select that allows users to pick any of the valid attributes to edit
		askAttributes := promptui.Select{
			Label: "Pick attributes of the Issue to edit",
			Items: []string{"continue", "Title", "Description", "Labels", "Confidential"},
		}
		_, resultAttributes, err := askAttributes.Run()
		if err != nil {
			return err
		}

		// Deal with each attribute
		switch resultAttributes {
		case "continue":
			break Continue
		case "Title":
			// Ask for Title
			askTitle := promptui.Prompt{
				Label: "Title",
				Validate: func(s string) error {
					if s == "" {
						return errors.New("Path must not be empty")
					}
					return nil
				},
			}
			resultTitle, err := askTitle.Run()
			if err != nil {
				fmt.Printf("%s failed to get Title due to %s\n", promptui.IconBad, err)
			}
			opts.Title = &resultTitle
		case "Description":
			// Use EditWithEditor to open description on one's text editor since it is multiline
			resultDescription, err := constants.EditWithEditor(nil)
			if err != nil {
				fmt.Printf("%s failed to get description due to %s\n", promptui.IconBad, err)
				continue
			} else if resultDescription == nil {
				fmt.Printf("%s description is empty, not changing\n", promptui.IconWarn)
				continue
			}
			resultDesc := strings.TrimSpace(*resultDescription)
			opts.Description = &resultDesc
		case "Labels":
			askLabels := promptui.Prompt{
				Label: "comma-separated list of prompts",
			}
			resultLabels, err := askLabels.Run()
			if err != nil {
				fmt.Printf("%s failed to get labels due to %s\n", promptui.IconBad, err)
				continue
			}
			labels := gitlab.Labels{resultLabels}
			opts.Labels = labels
		case "Confidential":
			askConfidential := promptui.Select{
				Label: "Make Merge Request confidential",
				Items: []string{"yes", "no"},
			}
			_, resultConfidential, err := askConfidential.Run()
			if err != nil {
				fmt.Printf("%s failed to get if Merge Request is confidential due to %s\n", promptui.IconBad, err)
				continue
			}
			var confidential bool
			if resultConfidential == "yes" {
				confidential = true
			} else {
				confidential = false
			}
			opts.Confidential = &confidential
		}
	}

	if opts.Title == nil || *opts.Title == "" {
		return fmt.Errorf("%s Title must be set", promptui.IconBad)
	}

	if dryRun {
		fmt.Printf("Title: %s\n", *opts.Title)
		if opts.Description != nil {
			for _, line := range strings.Split(*opts.Description, "\n") {
				fmt.Printf("Description: %s\n", line)
			}
		}
		if len(opts.Labels) != 0 {
			fmt.Printf("Labels: %s\n", opts.Labels)
		}
		if opts.Confidential == nil {
			fmt.Println("Confidential: false")
		} else {
			fmt.Printf("Confidential: %t", *opts.Confidential)
		}
		return nil
	}
	fmt.Printf("%s creating issue on %s\n", promptui.IconInitial, cBold.Sprint(repoSpec))
	res, _, err := GitlabClient.Issues.CreateIssue(
		repoSpec,
		opts,
		nil,
	)
	if err != nil {
		return err
	}
	fmt.Printf("%s created issue #%s on %s\n",
		promptui.IconGood,
		cBold.Sprint(res.IID),
		cBold.Sprint(repoSpec),
	)
	return nil
}
