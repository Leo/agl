package issue

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

// View an issue
var View = &cobra.Command{
	Use:   "view [IssueNumber]",
	Short: "View information about a project issue",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if err := viewIssue(args[0]); err != nil {
			fmt.Printf("%s %s\n", promptui.IconBad, err)
			os.Exit(1)
		}
	},
}

func viewIssue(input string) error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}

	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	issueNum, err := strconv.Atoi(input)
	if err != nil {
		return err
	}

	issue, _, err := GitlabClient.Issues.GetIssue(
		repoSpec,
		issueNum,
		nil,
	)
	if err != nil {
		return err
	}

	// TODO: This might be better the same as all the others, * at the start
	fmt.Printf("Issue #%d\n", issue.IID)

	fmt.Printf("* %s\n", issue.Title)

	switch issue.State {
	case "opened":
		fmt.Printf("%s is %s\n", cPositive.Sprint("*"), cPositive.Sprint("open"))
	case "closed":
		if issue.ClosedBy != nil {
			fmt.Printf("%s was %s by %s\n",
				cNegative.Sprint("*"),
				cNegative.Sprint("closed"),
				cUser.Sprint(issue.ClosedBy.Username),
			)
		} else {
			fmt.Printf("%s is %s\n", cNegative.Sprint("*"), cNegative.Sprint("closed"))
		}
	}

	fmt.Printf("* created by %s\n", cUser.Sprint(issue.Author.Username))

	fmt.Printf("* created on %s at %s\n",
		cTime.Sprint(issue.CreatedAt.Format("Monday 2 January 2006")),
		cTime.Sprint(issue.CreatedAt.Format("15:04:05")),
	)

	if !issue.CreatedAt.Equal(*issue.UpdatedAt) {
		fmt.Printf("* last updated on %s at %s\n",
			cTime.Sprint(issue.UpdatedAt.Format("Monday 2 January 2006")),
			cTime.Sprint(issue.UpdatedAt.Format("15:04:05")),
		)
	}

	if issue.Milestone != nil {
		fmt.Printf("* part of the %s milestone\n", cObject.Sprintf(issue.Milestone.Title))
	}

	if issue.Confidential {
		fmt.Printf("%s is %s\n", cNegative.Sprint("*"), cNegative.Sprint("confidential"))
	}

	if issue.Assignees != nil && helpers.ProfileGet("username") != issue.Author.Username {
		fmt.Print("* assigned to:\n")
		for _, assignee := range issue.Assignees {
			fmt.Printf("  %s\n", cUser.Sprint(assignee.Username))
		}
	} else if issue.Assignee != nil {
		fmt.Printf("* assigned to: %s\n", cUser.Sprint(issue.Assignee.Username))
	}

	if len(issue.Labels) > 0 {
		fmt.Printf("* has the labels:\n")
		for _, label := range issue.Labels {
			fmt.Printf("  %s\n", cObject.Sprintf(label))
		}
	}

	if issue.DiscussionLocked {
		fmt.Printf("%s discussion is %s\n", cNegative.Sprint("*"), cNegative.Sprint("locked"))
	}

	if issue.Subscribed {
		fmt.Printf("%s you're %s to this issue\n", cPositive.Sprint("*"), cPositive.Sprint("subscribed"))
	}

	if issue.Upvotes > 0 {
		fmt.Printf("%s has %s\n",
			cPositive.Sprint("*"),
			cPositive.Sprintf("%d upvotes", issue.Upvotes),
		)
	}

	if issue.Downvotes > 0 {
		fmt.Printf("%s has %s\n",
			cNegative.Sprint("*"),
			cNegative.Sprintf("%d downvotes", issue.Downvotes),
		)
	}

	if issue.UserNotesCount == 1 {
		fmt.Printf("* there is %s comment\n", cBold.Render(issue.UserNotesCount))
	} else if issue.UserNotesCount > 1 {
		fmt.Printf("* there are %s comments\n", cBold.Render(issue.UserNotesCount))
	}

	if issue.MergeRequestCount == 1 {
		fmt.Printf("* referenced by %s merge request\n", cBold.Render(issue.MergeRequestCount))
	} else if issue.MergeRequestCount > 1 {
		fmt.Printf("* referenced by %s merge requests\n", cBold.Render(issue.MergeRequestCount))
	}

	// It is imperative that this go at the end because it can be a massive wall of text
	if issue.Description != "" {
		fmt.Print("* Description:\n")
		for _, line := range strings.Split(issue.Description, "\n") {
			fmt.Printf("  %s\n", line)
		}
	}

	return nil
}
