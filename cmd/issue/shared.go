package issue

import "gitlab.alpinelinux.org/Leo/agl/constants"

var (
	dryRun bool

	cBold     = constants.ColorBold
	cUser     = constants.ColorUser
	cObject   = constants.ColorObject
	cPositive = constants.ColorPositive
	cNegative = constants.ColorNegative
	cTime     = constants.ColorTime
)
