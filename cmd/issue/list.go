package issue

import (
	"errors"
	"fmt"
	"os"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
	"golang.org/x/crypto/ssh/terminal"
)

var (
	issueListState        string
	issueListAssignee     string
	issueListLabel        string
	issueListMilestone    string
	issueListScope        string
	issueListConfidential bool
	issueListSort         string
	issueListSearch       string
	issueListIn           string
	// TODO: Support searching by author username, API supports via strings but go-gitlab doesn't
	// TODO: Support not which allows NOT matching some values like labels, API supports but go-gitlab doesn't
	// TODO: Support lower limits of issues, GitLab API fetches 20 as it is paginated

	// List issues
	List = &cobra.Command{
		Use:     "list",
		Aliases: []string{"search"},
		Short:   "list and filter issues",
		Args:    cobra.RangeArgs(0, 1),
		Run: func(cmd *cobra.Command, args []string) {
			if err := listIssues(); err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
		},
	}
)

func init() {
	List.Flags().StringVarP(&issueListState,
		"state",
		"s",
		"open",
		"Filter by state: {opened|closed|all}",
	)
	List.Flags().StringVarP(&issueListAssignee,
		"assignee",
		"a",
		"",
		"Filter by assignee",
	)
	List.Flags().StringVarP(&issueListLabel,
		"label",
		"l",
		"",
		"Filter by one or more (comma separated) labels",
	)
	List.Flags().StringVarP(&issueListMilestone,
		"milestone",
		"m",
		"",
		"Filter by the milestone title",
	)
	List.Flags().StringVarP(&issueListScope,
		"scope",
		"S",
		"all",
		"Filter by scope: {created-by-me|assigned-to-me|all}",
	)
	List.Flags().BoolVarP(&issueListConfidential,
		"confidential",
		"C",
		false,
		"Search only issues that are confidential",
	)
	List.Flags().StringVar(&issueListSort,
		"sort",
		"desc",
		"Sort in descending (or desc) or ascending (or asc) order",
	)
	List.Flags().StringVar(&issueListSearch,
		"search",
		"",
		"Search string against the title and/or description",
	)
	List.Flags().StringVar(&issueListIn,
		"in",
		"title,description",
		"Narrow down search: {title|description|title,description}",
	)
}

func listIssues() error {
	GitlabClient, err := helpers.GetClient()
	if err != nil {
		return err
	}

	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	filters := gitlab.ListProjectIssuesOptions{
		Confidential: &issueListConfidential,
	}

	switch issueListState {
	case "open", "":
		t := "opened"
		filters.State = &t
	case "closed", "all":
		filters.State = &issueListState
	default:
		return fmt.Errorf("%s is an invalid state to filter", issueListState)
	}

	if issueListAssignee != "" {
		filters.AssigneeUsername = &issueListAssignee
	}

	if issueListLabel != "" {
		filters.Labels = gitlab.Labels{issueListLabel}
	}

	if issueListMilestone != "" {
		filters.Milestone = &issueListMilestone
	}

	switch issueListScope {
	case "all", "":
		filters.Scope = &issueListScope
	case "assigned-to-me":
		t := "assigned_to_me"
		filters.Scope = &t
	case "created-by-me":
		t := "created_by_me"
		filters.Scope = &t
	default:
		return fmt.Errorf("%s is an invalid scope to filter", issueListScope)
	}

	switch issueListSort {
	case "ascending":
		t := "asc"
		filters.Sort = &t
	case "descending", "":
		t := "desc"
		filters.Sort = &t
	case "asc", "desc":
		filters.Sort = &issueListSort
	default:
		return fmt.Errorf("%s is an invalid sorting value", issueListSort)
	}

	if issueListSearch != "" {
		filters.Search = &issueListSearch
	}

	switch issueListIn {
	case "":
		t := "title,description"
		filters.In = &t
	case "title", "description", "title,description":
		filters.In = &issueListIn
	default:
		return fmt.Errorf("%s is an invalid narrower value", issueListIn)
	}

	// List Issues
	issues, _, err := GitlabClient.Issues.ListProjectIssues(
		repoSpec,
		&filters,
		nil,
	)
	if err != nil {
		return err
	}
	if len(issues) < 1 {
		return errors.New("no issues were found with your filter options")
	}

	width, _, err := terminal.GetSize(int(os.Stdin.Fd()))
	if err != nil {
		width = 80
	}

	// 80 is a sane default, 80 is what people commonly use but I think we can shoot for me
	if width < 80 {
		width = 80
	}

	for _, issue := range issues {
		expectedIIDSize := len(fmt.Sprintf("#%d | ", issue.IID))                     // Expected Size of IID block
		expectedUsernameSize := len(fmt.Sprintf("By: %s | ", issue.Author.Username)) // Expected size of By: block
		expectedTitleSize := len(fmt.Sprintf("Title: %s", issue))                    // Expected Size of Title block
		expectedFinalSize := expectedIIDSize + expectedUsernameSize + expectedTitleSize

		// Title that will be printed, subject to truncation
		finalTitle := issue.Title

		// Check if we have or not columns to spare
		if expectedFinalSize > width {
			// How many characters will be left out, add 3 to it for the '...', we should
			// have how many characters we need to truncate to fit into the screen after
			// adding the '...'
			maxLen := width - (expectedIIDSize + expectedUsernameSize + len(fmt.Sprint("Title: ")))
			finalTitle = helpers.TruncateString(finalTitle, maxLen)
		}

		fmt.Printf("#%s | By: %s | Title: %s\n",
			cBold.Render(issue.IID),
			cUser.Sprint(issue.Author.Username),
			cObject.Sprint(finalTitle),
		)
	}
	return nil
}
