package issue

import (
	"fmt"
	"os"
	"strconv"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

// Close an issue
var Close = &cobra.Command{
	Use:   "close <issuenum || URL>",
	Short: "close an issue",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		// Check if issue is actually num
		issueNum, err := strconv.Atoi(args[0])
		if err != nil {
			var repoSpec string
			repoSpec, issueNum, err = helpers.FindIssueViaURL(args[0])
			if err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
			viper.Set("picked_repo", repoSpec)
		}
		if err := closeIssue(issueNum); err != nil {
			fmt.Printf("%s %s\n", promptui.IconBad, err)
			os.Exit(1)
		}
	},
}

func init() {
	Close.Flags().BoolVarP(&dryRun, "dry-run", "n", false, "don't close the issue, show which it would close")
}

func closeIssue(issueNum int) error {
	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	state := "close"

	if dryRun {
		fmt.Printf("Would close issue #%s in %s\n",
			cBold.Sprint(issueNum),
			cBold.Sprint(repoSpec),
		)
		return nil
	}
	fmt.Printf("%s closing issue #%s in %s\n",
		promptui.IconInitial,
		cBold.Sprint(issueNum),
		cBold.Sprint(repoSpec),
	)
	err = updateIssue(repoSpec, issueNum, gitlab.UpdateIssueOptions{StateEvent: &state})
	if err != nil {
		return err
	}
	fmt.Printf("%s closed issue #%s in %s\n",
		promptui.IconGood,
		cBold.Sprint(issueNum),
		cBold.Sprint(repoSpec),
	)
	return nil
}
