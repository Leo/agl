package issue

import (
	"fmt"
	"os"
	"strconv"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/Leo/agl/helpers"
)

// Reopen an issue that was previously closed
var Reopen = &cobra.Command{
	Use:   "reopen <issuenum || URL>",
	Short: "reopen an issue",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		// Check if issue is actually num
		issueNum, err := strconv.Atoi(args[0])
		if err != nil {
			var repoSpec string
			repoSpec, issueNum, err = helpers.FindIssueViaURL(args[0])
			if err != nil {
				fmt.Printf("%s %s\n", promptui.IconBad, err)
				os.Exit(1)
			}
			viper.Set("picked_repo", repoSpec)
		}
		if err := reopenIssue(issueNum); err != nil {
			fmt.Printf("%s %s\n", promptui.IconBad, err)
			os.Exit(1)
		}
	},
}

func init() {
	Reopen.Flags().BoolVarP(&dryRun, "dry-run", "n", false, "don't close the issue, show which it would close")
}

func reopenIssue(issueNum int) error {
	repoSpec, err := helpers.GetRepoSpecFromArgs()
	if err != nil {
		return err
	}

	state := "reopen"

	if dryRun {
		fmt.Printf("Would reopen issue #%s in %s\n",
			cBold.Sprint(issueNum),
			cBold.Sprint(repoSpec),
		)
		return nil
	}
	fmt.Printf("%s reopening issue #%s in %s\n",
		promptui.IconInitial,
		cBold.Sprint(issueNum),
		cBold.Sprint(repoSpec),
	)
	err = updateIssue(repoSpec, issueNum, gitlab.UpdateIssueOptions{StateEvent: &state})
	if err != nil {
		return err
	}
	fmt.Printf("%s reopened issue #%s in %s\n",
		promptui.IconGood,
		cBold.Sprint(issueNum),
		cBold.Sprint(repoSpec),
	)
	return nil
}
