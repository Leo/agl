package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.alpinelinux.org/Leo/agl/cmd/mr"
)

var mrCmd = &cobra.Command{
	Use:   "mr",
	Short: "manage pull requests",
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

func init() {
	rootCmd.AddCommand(mrCmd)

	mrCmd.AddCommand(mr.Create)
	mrCmd.AddCommand(mr.List)
	mrCmd.AddCommand(mr.Checkout)
	mrCmd.AddCommand(mr.Edit)
	mrCmd.AddCommand(mr.Reopen)
	mrCmd.AddCommand(mr.Close)
	mrCmd.AddCommand(mr.Approve)
	mrCmd.AddCommand(mr.Checks)
	mrCmd.AddCommand(mr.View)
	mrCmd.AddCommand(mr.Changes)
	mrCmd.AddCommand(mr.WIP)
	mrCmd.AddCommand(mr.Ready)
}
