package helpers

import (
	"testing"
)

func TestTruncateString(t *testing.T) {
	t.Parallel()
	got := TruncateString("abcdefg", 4)
	if got != "a..." {
		t.Errorf("expected a... got %s", got)
	}
}

func TestTruncateStringShort(t *testing.T) {
	t.Parallel()
	got := TruncateString("abc", 1)
	if got != "a..." {
		t.Errorf("expected a... got %s", got)
	}
}

func TestTruncateStringNotMaxLen(t *testing.T) {
	t.Parallel()
	got := TruncateString("abc", 7)
	if got != "abc" {
		t.Errorf("expected abc got %s", got)
	}
}

func TestGetNamespacePathFromHTTPSes(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		desc string
		in   string
		out  string
	}{
		{"simple URL", "https://gitlab.alpinelinux.org/Leo/agl", "Leo/agl"},
		{"simple URL with .git", "https://gitlab.alpinelinux.org/Leo/agl.git", "Leo/agl"},

		{"Missing one component", "https://gitlab.org/Leo/agl", "Leo/agl"},
		{"Missing one component with .git", "https://gitlab.org/Leo/agl.git", "Leo/agl"},
		{"Missing one component with extra slashes", "https://gitlab.org///Leo///agl//", "Leo/agl"},

		{"Missing Schema", "gitlab.alpinelinux.org/Leo/agl", "Leo/agl"},
		{"Missing Schema with .git", "gitlab.alpinelinux.org/Leo/agl.git", "Leo/agl"},
		{"Missing Schema with extra slashes", "gitlab.alpinelinux.org//Leo///agl/", "Leo/agl"},

		{"Missing top-level domain", "https://gitlab.alpinelinux/Leo/agl", "Leo/agl"},
		{"Missing top-level domain with .git", "https://gitlab.alpinelinux/Leo/agl.git", "Leo/agl"},
		{"Missing top-level domain with extra slashes", "https://gitlab.alpinelinux//Leo//agl//", "Leo/agl"},

		{"Missing host", "https://org/Leo/agl", "Leo/agl"},
		{"Missing host with .git", "https://org/Leo/agl.git", "Leo/agl"},
		{"Missing host with extra slashes", "https://org///Leo/agl//", "Leo/agl"},

		{"Missing host and schema", "org/Leo/agl", "Leo/agl"},
		{"Missing host and schema with .git", "org/Leo/agl.git", "Leo/agl"},
		{"Missing host and schema with extra slashes", "//org///Leo///agl//", "Leo/agl"},

		{"Missing everything but Path", "Leo/agl", "Leo/agl"},
		{"Missing everything but Path with .git", "Leo/agl.git", "Leo/agl"},
		{"Missing everything but Path with extra slashes", "//Leo///agl//", "Leo/agl"},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			t.Parallel()
			got, err := GetNamespacePathFromURL(tC.in)
			if err != nil {
				t.Errorf("unexpected error %s", err)
			}
			if got != tC.out {
				t.Errorf("expected %s got %s", tC.out, got)
			}
		})
	}
}
func TestGetNamespacePathFromSSH(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		desc string
		in   string
		out  string
	}{
		{"simple URL", "git@gitlab.alpinelinux.org:Leo/agl", "Leo/agl"},
		{"simple URL with extra slashes", "git@gitlab.alpinelinux.org://Leo//agl.git//", "Leo/agl"},

		{"Missing one component", "git@gitlab.org:Leo/agl", "Leo/agl"},
		{"Missing one component with extra slashes", "git@gitlab.org:///Leo///agl//", "Leo/agl"},

		{"Missing Schema", "gitlab.alpinelinux.org:Leo/agl", "Leo/agl"},
		{"Missing Schema with extra slashes", "gitlab.alpinelinux.org://Leo///agl/", "Leo/agl"},

		{"Missing top-level domain", "git@gitlab.alpinelinux:/Leo/agl", "Leo/agl"},
		{"Missing top-level domain with extra slashes", "git@gitlab.alpinelinux://Leo//agl//", "Leo/agl"},

		{"Missing host", "git@org:/Leo/agl", "Leo/agl"},
		{"Missing host with extra slashes", "git@org/://Leo/agl//", "Leo/agl"},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			t.Parallel()
			got, err := GetNamespacePathFromURL(tC.in)
			if err != nil {
				t.Errorf("unexpected error %s", err)
			}
			if got != tC.out {
				t.Errorf("expected %s got %s", tC.out, got)
			}
		})
	}
}

func TestAlpinePrefix(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		desc string
		in   string
		out  string
	}{
		// Most simple cases
		{"no-hyphen", "3.12_alpine-sdk", "master"},
		{"pkgname itself", "alpine-sdk", "master"},
		{"master", "master", "master"},
		// Each version
		{"3.13", "3.13-alpine-sdk", "3.13-stable"},
		{"3.12", "3.12-alpine-sdk", "3.12-stable"},
		{"3.11", "3.11-alpine-sdk", "3.11-stable"},
		{"3.10", "3.10-alpine-sdk", "3.10-stable"},
		{"3.9", "3.9-alpine-sdk", "3.9-stable"},
		// Invalids with special situations
		{"hyphen-instead-of-dot", "3-9-alpine-sdk", "master"},
		{"only-dashes", "----", "master"},
		{"nothing", "", "master"},
		{"underlines", "3.13_alpine-sdk", "master"},
		{"at-as-sep", "3.13@alpine-sdk", "master"},
		{"exclamation-as-sep", "3.13!alpine-sdk", "master"},
		{"hash-as-sep", "3.13#alpine-sdk", "master"},
		{"asterisk-as-sep", "3.13*alpine-sdk", "master"},
		{"percent-as-sep", "3.13%alpine-sdk", "master"},
		{"brackets-as-sep", "3.13{}alpine-sdk", "master"},
		{"parentheses-as-sep", "3.13()alpine-sdk", "master"},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			t.Parallel()
			got := AlpinePrefix(tC.in)
			if got != tC.out {
				t.Errorf("expected %s got %s", tC.out, got)
			}
		})
	}
}

func TestConvertToHTTPS(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		desc string
		in   string
		out  string
	}{
		// Most simple cases
		{"git", "git://gitlab.alpinelinux.org/Leo/agl.git", "https://gitlab.alpinelinux.org/Leo/agl"},
		{"ssh", "git@gitlab.alpinelinux.org:Leo/agl.git", "https://gitlab.alpinelinux.org/Leo/agl"},
		{"ssh-with-leading-slash", "git@gitlab.alpinelinux.org:/Leo/agl.git", "https://gitlab.alpinelinux.org/Leo/agl"},
		{"http", "http://gitlab.alpinelinux.org/Leo/agl.git", "https://gitlab.alpinelinux.org/Leo/agl"},
	}
	for _, tC := range testCases {
		tC := tC
		t.Run(tC.desc, func(t *testing.T) {
			t.Parallel()
			got, err := ConvertToHTTPS(tC.in)
			if err != nil {
				t.Errorf("input %s gave unexpected error %s", tC.in, err)
			}
			if got != tC.out {
				t.Errorf("expected %s got %s", tC.out, got)
			}
		})
	}
}

func TestFindIssueViaURL(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		desc     string
		in       string
		repoSpec string
		num      int
	}{
		{"normal", "https://gitlab.alpinelinux.org/Leo/agl/-/issues/1", "Leo/agl", 1},
		{"no-schema", "gitlab.alpinelinux.org/Leo/agl/-/issues/1", "Leo/agl", 1},
		{"only-path", "Leo/agl/-/issues/1", "Leo/agl", 1},
	}
	for _, tC := range testCases {
		tC := tC
		t.Run(tC.desc, func(t *testing.T) {
			t.Parallel()
			gotRepoSpec, gotNum, err := FindIssueViaURL(tC.in)
			if err != nil {
				t.Errorf("input %s gave unexpected error %s", tC.in, err)
			}
			if gotRepoSpec != tC.repoSpec {
				t.Errorf("expected repoSpec %s got %s", tC.repoSpec, gotRepoSpec)
			}
			if gotNum != tC.num {
				t.Errorf("expected number %d got %d", tC.num, gotNum)
			}
		})
	}
}

func TestFindMergeRequestViaURL(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		desc     string
		in       string
		repoSpec string
		num      int
	}{
		{"normal", "https://gitlab.alpinelinux.org/Leo/agl/-/issues/1", "Leo/agl", 1},
		{"no-schema", "gitlab.alpinelinux.org/Leo/agl/-/issues/1", "Leo/agl", 1},
		{"only-path", "Leo/agl/-/issues/1", "Leo/agl", 1},
	}
	for _, tC := range testCases {
		tC := tC
		t.Run(tC.desc, func(t *testing.T) {
			t.Parallel()
			gotRepoSpec, gotNum, err := FindMergeRequestViaURL(tC.in)
			if err != nil {
				t.Errorf("input %s gave unexpected error %s", tC.in, err)
			}
			if gotRepoSpec != tC.repoSpec {
				t.Errorf("expected repoSpec %s got %s", tC.repoSpec, gotRepoSpec)
			}
			if gotNum != tC.num {
				t.Errorf("expected number %d got %d", tC.num, gotNum)
			}
		})
	}
}
