package helpers

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"

	git "github.com/libgit2/git2go/v31"
	"github.com/spf13/viper"
	giturls "github.com/whilp/git-urls"
	"github.com/xanzy/go-gitlab"
)

// GetNamespacePathFromURL takes a string that is a URL and parses it and returns
// a string matching a NAMESPACE/PATH as used by Gitlab.
//
// It also removes the .git suffix if it is present so it is OK to use on remotes
func GetNamespacePathFromURL(URL string) (string, error) {
	var repoSpec []string

	// trim the URL out of unimportant or faulty components, this is to avoid
	// people tricking us by passing URLs which a ridiculous amount of '/' and
	// also to automatically deal with SSH URLs
	URL = trimURL(URL)

	// Replace .git at the end
	re := regexp.MustCompile("\\.git$")
	URL = re.ReplaceAllString(URL, "")

	// Split the Path component by '/', we should get the last 2
	splitPath := strings.Split(URL, "/")

	if len(splitPath) < 2 {
		return "", errors.New("bad URL, expected at least 2 components")
	}

	repoSpec = append(repoSpec, splitPath[len(splitPath)-2]) // Second to last element
	repoSpec = append(repoSpec, splitPath[len(splitPath)-1]) // Last element

	// Check if the second to last element contains ':', this is part of an SSH URL, so split
	// based on it and get the last element
	if strings.Contains(repoSpec[0], ":") {
		splittedSSH := strings.Split(repoSpec[0], ":")
		repoSpec[0] = splittedSSH[len(splittedSSH)-1]
	}

	repoSpecString := strings.Join(repoSpec, "/")

	return repoSpecString, nil
}

// GitCredentials provides authentication via the Personal Access Token
// TODO: SSH is currently broken and I have no will to knowledge of how to fix it
func GitCredentials(url string, username string, allowedTypes git.CredType) (*git.Cred, error) {
	var creds *git.Cred
	var err error
	switch allowedTypes {
	case git.CredTypeUserpassPlaintext:
		creds, err = git.NewCredUserpassPlaintext(
			username,
			viper.GetString("token"),
		)
		if err != nil {
			return nil, err
		}
	case git.CredTypeSshKey:
		creds, err = git.NewCredSshKeyFromAgent(username)
		if err != nil {
			return nil, err
		}
	}
	return creds, nil

}

func GitCertificates(cert *git.Certificate, valid bool, hostname string) git.ErrorCode {
	if hostname != "gitlab.alpinelinux.org" {
		return git.ErrCertificate
	}
	return git.ErrOk
}

// SwitchToBranch switches the repository to reference a new branch
func SwitchToBranch(r *git.Repository, branch string) (*git.Reference, error) {
	// Look up the branch we were given and err if it doesn't exist
	// this just checks if the branch exists locally
	_, err := r.LookupBranch(branch, git.BranchLocal)
	if err != nil {
		return nil, err
	}

	// Make HEAD reference our branch
	// The true is absolutely necessary
	_, err = r.References.CreateSymbolic("HEAD", "refs/heads/"+branch, true, "")
	if err != nil {
		return nil, err
	}

	err = r.CheckoutHead(&git.CheckoutOpts{
		Strategy: git.CheckoutForce,
	})
	if err != nil {
		return nil, err
	}
	ref, err := r.Head()
	if err != nil {
		return nil, err
	}
	return ref, nil
}

// PerformRebaseOnto takes a repository, the name of a branch to perform the rebase onto
// a second branch, it will put the changes of branch 'in' onto the branch.
//
// Example: PerformRebaseOnto(repo, "i3wm", "master") will rebase the branch "i3wm" onto
// the "master" branch, which is effectively `git rebase i3wm master`, it also
// accepts `remote/` like `upstream/master`.
//
// This function is taken from libgit2/git2go test_rebase.go
func PerformRebaseOnto(r *git.Repository, in string, onto string) (*git.Rebase, error) {
	// Look the branch from where we are going to get the changes from
	inBranch, err := r.LookupBranch(in, git.BranchRemote)
	if err != nil {
		return nil, err
	}
	defer inBranch.Free()

	// Get the commit reference
	inCommit, err := r.AnnotatedCommitFromRef(inBranch.Reference)
	if err != nil {
		return nil, err
	}
	defer inCommit.Free()

	// Look the branch we are going to commit into
	ontoBranch, err := r.LookupBranch(onto, git.BranchLocal)
	if err != nil {
		return nil, err
	}
	defer ontoBranch.Free()

	// Get the commit reference
	ontoCommit, err := r.AnnotatedCommitFromRef(ontoBranch.Reference)
	if err != nil {
		return nil, err
	}
	defer ontoCommit.Free()

	// Init rebase, Rebase the changes fromCommit relative to ontoCommit (everything
	// that fromCommit changed) into ontoCommit
	rebase, err := r.InitRebase(ontoCommit, nil, inCommit, nil)
	if err != nil {
		return nil, err
	}

	// Check no operation has been started yet
	rebaseOperationIndex, err := rebase.CurrentOperationIndex()
	if rebaseOperationIndex != git.RebaseNoOperation && err != git.ErrRebaseNoOperation {
		return rebase, errors.New("No operation should have been started yet")
	}

	// Iterate in rebase operations regarding operation count
	opCount := int(rebase.OperationCount())
	for op := 0; op < opCount; op++ {
		operation, err := rebase.Next()
		if err != nil {
			return rebase, err
		}

		// Check operation index is correct
		rebaseOperationIndex, err = rebase.CurrentOperationIndex()
		if int(rebaseOperationIndex) != op {
			return rebase, errors.New("Bad operation index")
		}
		if !operationsAreEqual(rebase.OperationAt(uint(op)), operation) {
			return rebase, errors.New("Rebase operations should be equal")
		}

		// Get current rebase operation created commit
		commit, err := r.LookupCommit(operation.Id)
		if err != nil {
			return rebase, err
		}
		defer commit.Free()

		// Apply commit
		err = rebase.Commit(operation.Id, commit.Author(), commit.Committer(), commit.Message())
		if err != nil {
			return rebase, err
		}
	}

	return rebase, nil
}

func operationsAreEqual(l, r *git.RebaseOperation) bool {
	return l.Exec == r.Exec && l.Type == r.Type && l.Id.String() == r.Id.String()
}

// GetRemotePathSpec returns the PathSpec of remote, returns nil if any error occurs
func GetRemotePathSpec(remote string) string {
	// Open the repository that we are currently in, we use it to find remotes
	r, err := git.OpenRepository(".") // PWD
	if err != nil {
		return ""
	}
	remoteRef, err := r.Remotes.Lookup(remote)
	if err != nil {
		return ""
	}
	pathSpec, err := GetNamespacePathFromURL(remoteRef.Url())
	if err != nil {
		return ""
	}
	return pathSpec
}

// GetRepoSpecFromArgs checks if a repoSpec can be extracted from args via URL,
// emptyness (check git origin remote for URL) or by being given the repoSpec itself
func GetRepoSpecFromArgs() (string, error) {
	// Load picked_repo from Viper
	input := viper.GetString("picked_repo")

	// Check if we were given no args and try to guess from a series of remotes
	// in the git repo
	if input == "" {
		// Try the picked_remote which is set via the global --remote flag
		if remote := viper.GetString("picked_remote"); remote != "" {
			if repoSpec := GetRemotePathSpec(remote); repoSpec != "" {
				return repoSpec, nil
			}
		}
		if repoSpec := GetRemotePathSpec("gitlab"); repoSpec != "" {
			return repoSpec, nil
		}
		if repoSpec := GetRemotePathSpec("upstream"); repoSpec != "" {
			return repoSpec, nil
		}
		if repoSpec := GetRemotePathSpec("origin"); repoSpec != "" {
			return repoSpec, nil
		}
		return "", errors.New("invalid usage: couldn't find any remote to find the target Project URL")
	}

	// Split by '/' and check if the length is exactly 2, it means we were given a
	// repoSpec (NAMESPACE/PATH)
	inputSplit := strings.Split(input, "/")
	inputLen := len(inputSplit)

	if inputLen > 2 {
		// Most likely given an URL, use getNamespacePathFromURL
		repoSpec, err := GetNamespacePathFromURL(input)
		if err != nil {
			return "", err
		}
		return repoSpec, nil
	} else if inputLen == 2 {
		// We were give /PATH, get the username from viper and return it
		if inputSplit[0] == "" {
			return fmt.Sprintf("%s/%s", viper.GetString("user.username"), inputSplit[1]), nil
		}
		// We were given NAMESPACE/PATH, we can just return the input
		if inputSplit[0] != "" && inputSplit[1] != "" {
			return input, nil
		}
	} else if inputLen == 1 {
		return fmt.Sprintf("%s/%s", viper.GetString("user.username"), input), nil
	}

	return "", fmt.Errorf("value '%s' is invalid", input)
}

// TruncateString takes a string and a length and truncates at length -3 replacing with ...
func TruncateString(str string, maxLen int) string {
	// Remove 3 from max length to make space for the '...'
	if maxLen > 3 {
		maxLen -= 3
	}
	truncated := ""
	count := 0
	for _, char := range str {
		truncated += string(char)
		count++
		if count >= maxLen {
			break
		}
	}
	// If count is under maxLen then nothing was truncated, return the string as-is
	if count < maxLen {
		return truncated
	}
	// The string was truncated return it with ...
	return truncated + "..."

}

// FindIssueViaURL checks a URL for the path to a gitlab issue, returns the NAMESPACE/PATH, the issue number
// and an error if there is any
func FindIssueViaURL(URL string) (string, int, error) {
	URL = trimURL(URL)
	splitURL := strings.Split(URL, "/")

	if len(splitURL) < 5 {
		return "",
			0,
			fmt.Errorf("invalid URL: %s must have at least 5 components, /NAMESPACE/PATH/-/issues/ISSUENUM", URL)
	}

	issueNum, err := strconv.Atoi(splitURL[len(splitURL)-1])
	if err != nil {
		return "", 0, fmt.Errorf("invalidURL: %s must have an integer at the end", URL)
	}

	repoSpec := fmt.Sprintf("%s/%s",
		splitURL[len(splitURL)-5], // NAMESPACE
		splitURL[len(splitURL)-4], // PATH
	)

	return repoSpec, issueNum, nil
}

// FindMergeRequestViaURL checks a URL for the path to a gitlab issue, returns the NAMESPACE/PATH, the issue number
// and an error if there is any
func FindMergeRequestViaURL(URL string) (string, int, error) {
	URL = trimURL(URL)
	splitURL := strings.Split(URL, "/")

	if len(splitURL) < 5 {
		return "",
			0,
			fmt.Errorf("invalid URL: %s must have at least 5 components, /NAMESPACE/PATH/-/merge_requests/ISSUENUM", URL)
	}

	issueNum, err := strconv.Atoi(splitURL[len(splitURL)-1])
	if err != nil {
		return "", 0, fmt.Errorf("invalidURL: %s must have an integer at the end", URL)
	}

	repoSpec := fmt.Sprintf("%s/%s",
		splitURL[len(splitURL)-5], // NAMESPACE
		splitURL[len(splitURL)-4], // PATH
	)

	return repoSpec, issueNum, nil
}

// GitCommitIterateUntil takes a git repository and a commit to find, it walks from the head commit
// of the checked out branch until it finds the commit or the end of the repo
//
// This is meant to get all commits from a branch after it has been rebased on top of another branch
// assume we are in the current good branch
func GitCommitIterateUntil(
	r *git.Repository,
	baseID *git.Oid,
) (
	[]string,
	map[string]git.Commit,
	error,
) {
	// Return this with all commit titles
	var commitTitles []string
	// Return this with commit title matching a *git.Commit
	commits := make(map[string]git.Commit)

	// Get the ID of the tip commit
	headCommit, err := r.Head()
	tipCommit, err := r.LookupCommit(headCommit.Target())
	if err != nil {
		return commitTitles, commits, err
	}

	currentCommit := tipCommit
	for {
		if currentCommit == nil {
			break // We somehow went through all the commits until the start of the tree and didn't find it
		}
		currentCommitID := currentCommit.Id()
		if currentCommitID.Equal(baseID) {
			break
		}
		// Get the title of the commit by splitting the message by line and getting the first element
		cTitle := strings.Split(currentCommit.Message(), "\n")[0]

		// Append the title to the array
		commitTitles = append(commitTitles, cTitle)

		// Add a key with the value of the title to the array, point to the git.Commit object
		commits[cTitle] = *currentCommit

		// Go to the parent commit, we must eventually reach it but if not we will bail once this becomes nil4
		currentCommit = currentCommit.Parent(0)
	}

	return commitTitles, commits, nil
}

// AlpinePrefix checks if a string has an alpine prefix and return the prefix or master
func AlpinePrefix(str string) string {
	// All alpine prefixes need a hyphen, so check if we have one
	// also convenient as we will be splitting this
	if !strings.Contains(str, "-") {
		return "master"
	}

	prefix := strings.Split(str, "-")[0]
	// split by the hyphen and gather the first
	switch prefix {
	case "3.9", "3.10", "3.11", "3.12", "3.13":
		return fmt.Sprintf("%s-stable", prefix)
	default:
		return "master"
	}
}

// ConvertToHTTPS convers a git URL into its HTTPS equivalent
// e.g. ssh://git@gitlab.alpinelinux.org/alpine/aports -> https://gitlab.alpinelinux.org/alpine/aports
func ConvertToHTTPS(rawurl string) (string, error) {
	u, err := giturls.Parse(rawurl)
	if err != nil {
		return "", err
	}

	switch u.Scheme {
	case "ssh":
		// Remove user information which is the 'git@' part
		u.User = nil
	case "https":
		return rawurl, nil
	}

	// Switch the scheme to https
	u.Scheme = "https"

	q := u.Query()
	u.RawQuery = q.Encode()

	URL := u.String()

	re := regexp.MustCompile("\\.git$")
	URL = re.ReplaceAllString(URL, "")

	return URL, nil
}

// ProfileGet gets the value of a key in relation to the profile being currently used
// it might return nil to beware
func ProfileGet(key string) string {
	// This relies on us doing viper.Set("profile", profile) at the end of initConfig
	// on cmd/root.go
	profile := viper.GetString("profile")

	supViper := viper.Sub("profiles." + profile)

	return supViper.GetString(key)
}

// GetClient returns a gitlab.Client using the proper host and token
func GetClient() (*gitlab.Client, error) {
	GitlabClient, err := gitlab.NewClient(
		ProfileGet("token"),
		gitlab.WithBaseURL(fmt.Sprintf("https://%s", ProfileGet("host"))),
	)
	if err != nil {
		return nil, err
	}
	return GitlabClient, nil
}

func trimURL(URL string) string {
	// SSH, split by the ':' and get the last component, SSH URLs use ':' as the separator
	// for path like git@gitlab.alpinelinux.org:Leo/agl
	if strings.HasPrefix(URL, "git@") {
		splitURL := strings.Split(URL, ":")
		URL = splitURL[len(splitURL)-1]
	}

	// Catch any instance that has more than 1 backslash and replace it with only one, then
	// split the string, it shouldn't matter that we match 'https://' since URLs do not
	// necessarily have the schema and we don't care about the schema, just the last 2
	// components
	re := regexp.MustCompile(`//+`)
	URL = re.ReplaceAllString(URL, "/")
	re = regexp.MustCompile("^/")
	URL = re.ReplaceAllString(URL, "")
	re = regexp.MustCompile("/$")
	URL = re.ReplaceAllString(URL, "")

	return URL
}
