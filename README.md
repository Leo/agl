Alpine linux GitLab
===================

TODO
----

> Some incomplete root tick boxes will have sub tick boxes when work starts on
> implement its various components

- [ ] Merge Requests
  - [x] checkout
    - [x] via merge request internal id
    - [x] via merge request target branch
    - [x] via merge request URL
    - [ ] private/internal repos (can't sign-in via raw GET request)
  - [x] create (SOME STILL TODO BUT MOSTLY DONE)
    - [x] alpine/aports special handling
    - [x] basic functionality
    - [x] set attributes at creation time
      - [x] title
      - [x] source branch
      - [x] source Repo
      - [x] target branch
      - [x] target repo
      - [x] description
      - [x] labels
      - [ ] assignee_id(s) (WONTFIX, only can do via ID, GitLab should fix API)
      - [ ] milestone_id (WONTFIX, only can do via ID, GitLab should fix API)
      - [ ] allow_colaboration (TODO)
      - [x] allow_maintainer_to_push (Alpine Linux force-enables it)
      - [ ] squash (TODO, NOTPRIO since Alpine Linux doesn't squash)
  - [x] List|search (supports anything that go-gitlab supports nicely)
  - [x] edit (some elements need better support on go-gitlab)
  - [ ] diff (view diff)
  - [x] checks (show CI pipeline status)
  - [x] close (to be built on top of edit)
  - [x] reopen (to be built on top of edit)
  - [x] approve
  - [x] view
- [x] Issues
  - [x] close (built on top of edit)
  - [x] reopen (built on top of edit)
  - [x] edit (not all values, GitLab API)
  - [x] create
    - [x] basic functionality
    - [x] set attributes at creation time
      - [x] title
      - [x] description
      - [x] confidentiality
      - [x] labels
      - [ ] assign to users (WONTFIX, only can do via ID, GitLab should fix API)
      - [ ] milestones (WONTFIX, can only do via ID, GitLab should fix API)
  - [x] list (missing a few things not supported by GitLab API or go-gitlab)
  - [x] view
- [ ] Releases
  - [x] create
    - [x] basic functionality
    - [x] instead of Git Tags, use Git Commits and apply a Git Tag on them
    - [x] Edit attributes (except ones related to Git  Commits)
    - [x] Add assets
  - [x] list
  - [x] view
  - [x] delete
  - [ ] upload assets
  - [ ] download assets and auto-generated source
- [ ] Projects (repos)
  - [x] clone
  - [x] create (can create and asks for path and name but not many other options)
  - [x] delete
  - [x] fork
  - [ ] view (things like commits and other metadata like permissions)
- [ ] authentication (no API for asking an ouath token after login, possible if going raw)
- [x] shell completion (auto-generated but should be good enough)
- [x] config
  - [x] set
  - [x] get
  - [x] lint
  - [x] update (automatically call GitLab API to update profile configuration)

LONG TERM
---------

- [x] Make it able to use any GitLab instance (Done with profiles)
- [x] Make it able to pick user, per instance (Done with profiles)
- [ ] Write a styler for promptui so everything looks better
- [ ] Provide TUI to comment on issues
- [ ] dissect GitLab to allow creating personal access token from the commandline
- [ ] Make SSH as callback method work
- [ ] Rewrite rebasing logic in cmd/mr/create to abort gracefully on conflicts
- [ ] write helper to write to the config file safely by wiping and restoring temporary variables
- [ ] rewrite error handling to give a better error message if a merge request does not exist
