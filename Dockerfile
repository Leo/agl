FROM alpine:edge AS build-stage
MAINTAINER Leo <thinkabit.ukim@gmail.com>

RUN apk add -U --no-cache \
    alpine-sdk \
    cmake \
    ninja \
    go \
    openssl-dev \
    openssl-libs-static \
    libssh2-dev \
    libssh2-static \
    zlib-dev \
    zlib-static

RUN curl -L https://github.com/libgit2/libgit2/releases/download/v1.1.0/libgit2-1.1.0.tar.gz \
    -o libgit2-1.1.0.tar.gz \
    && tar xf libgit2-1.1.0.tar.gz

RUN cd libgit2-1.1.0 \
    && cmake -G Ninja -B build \
        -DBUILD_SHARED_LIBS=OFF \
        -DBUILD_CLAR=OFF \
        -DCMAKE_INSTALL_LIBDIR=lib \
        -DCMAKE_BUILD_TYPE=Release \
        -DUSE_BUNDLED_ZLIB=OFF \
        -DUSE_HTTP_PARSER=builtin \
        -DUSE_SSH=ON \
    && ninja -C build install

ENV GOOS=linux \
    GOARCH=amd64

COPY . /app

RUN cd app && go build -v -tags static,system_libgit2 -ldflags '-w -s -linkmode external -extldflags -static' \
    -o /dist/agl

FROM scratch as export-stage
COPY --from=build-stage /dist/agl /agl

CMD ["/agl"]
